<?php
namespace PortBundle\RuleManager;

interface Rule
{
	public function apply($value);
}