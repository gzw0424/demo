<?php
namespace PortBundle\RuleManager;

class GreaterThanRule implements Rule
{
	public function apply($value)
	{
		return $value > 4000;
	}
}