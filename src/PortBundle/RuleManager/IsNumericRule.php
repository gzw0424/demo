<?php
namespace PortBundle\RuleManager;

class IsNumericRule implements Rule
{
	public function apply($value)
	{
		return is_int($value);
	}
}