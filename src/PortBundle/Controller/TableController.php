<?php

namespace PortBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Psr\Log\LoggerInterface;
use AppBundle\Service\User;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\HttpFoundation\Request;
use PortBundle\Service\MessageGeneratorService;

class TableController extends Controller{
	public function exportAction($bundle,$entity,$filename){
		$entity = ucfirst($bundle).'Bundle\\Entity\\'.ucfirst($entity);
		// $path 为 '../file/stream-file.xls'
		$phpExcelObject = $this->get('phpexcel')->createPHPExcelObject('../file/'.$filename);
		$sheet = $phpExcelObject->getSheet();
		$highestRow = $sheet->getHighestRow();
		$highestColumn = $sheet->getHighestColumn();
		// 获取一行的数据
		for ($currentRow = 1; $currentRow <= $highestRow; $currentRow++) {
			$obj = new $entity();
			$data = array_keys($obj->toArr());
			array_shift($data);
			for ($currentCol = 0; $currentCol < ord($highestColumn)-64; $currentCol++){
				call_user_func(
						// 調用的方法
						[$obj,'set'.ucfirst($data[$currentCol])],
						// 方法的參數
						$sheet->getCell(chr($currentCol+65).$currentRow)->getValue());
			}
			$em = $this->getDoctrine()->getManager();
			$em->persist($obj);
			try{
				$em->flush();
			}catch (\Exception $e){
				return new Response('文件中的数据有重复的用户名（在用户表中），请修改文件');
			}
			return $this->redirectToRoute("blog_user_index");
		}
	}
	public function importAction($bundle,$entity,$filename,LoggerInterface $logger){
		$phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
		
		$phpExcelObject->getProperties()->setCreator("liuggio")
			->setLastModifiedBy("Giulio De Donato")
			->setTitle("Office 2005 XLSX Test Document")
			->setSubject("Office 2005 XLSX Test Document")
			->setDescription("Test document for Office 2005 XLSX, generated using PHP classes.")
			->setKeywords("office 2005 openxml php")
			->setCategory("Test result file");
		
		$em = $this->getDoctrine()->getManager();
		$users = $em->getRepository(ucfirst($bundle).'Bundle:'.ucfirst($entity))->findAll();
		
		for ($i=0; $i<count($users); $i++){
			$j = 0;
			$arr = $users[$i]->toArr();
			array_shift($arr);
			foreach ($arr as $value){
				$phpExcelObject->setActiveSheetIndex(0)->setCellValue(chr($j+65).($i+1), $value);
				$j++;
			}
		}
		
		$phpExcelObject->getActiveSheet()->setTitle('Simple');
		$phpExcelObject->setActiveSheetIndex(0);
		$writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
		$response = $this->get('phpexcel')->createStreamedResponse($writer);
		$dispositionHeader = $response->headers->makeDisposition(
				ResponseHeaderBag::DISPOSITION_ATTACHMENT,
				$filename
				);
		
		$response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
		$response->headers->set('Pragma', 'public');
		$response->headers->set('Cache-Control', 'maxage=1');
		$response->headers->set('Content-Disposition', $dispositionHeader);
		
		$logger->info('I just got the logger');
		$logger->error('An error occurred');
		$logger->critical('I left the oven on!', array(
				'cause' => 'in_hurry',
		));
		return $response;
	}
	public function indexAction(){
		$i = $this->get('abc');
		return new Response($i->getMailer());
// 		return $this->render('PortBundle:Table:index.html.twig',[
// 			'bundle' => 'blog',
// 			'entity' => 'user',
// 			'filename' => 'stream-file.xls'
// 		]);
	}
}