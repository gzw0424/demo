<?php

namespace PortBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BlogBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;

class LoggleController extends Controller
{
	public function indexAction()
	{
		$user = new User();
		$user->setUsername('s');
		$validator= $this->get('validator');
		$error = $validator->validate($user);
		return $this->render('PortBundle:Loggle:index.html.twig',[
			'errors' => $error
		]);
	}
}