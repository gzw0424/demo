<?php
namespace PortBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends Controller
{
	public function loginAction(Request $request,AuthenticationUtils $authUtils){
		return $this->render('PortBundle:Security:login.html.twig');
	}
}
