<?php

namespace PortBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TableCommand extends ContainerAwareCommand
{
    protected function configure()
    {
    	$this
	    	->setName('port:import')
	    	->setDescription('把一个表导入到user表中')
	    	->addArgument('p', InputArgument::OPTIONAL, '文件的路径')
	    	->addOption('path', null, InputOption::VALUE_NONE, '文件的路径')
	    	;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
    	/**
    	 * @var string $path
    	 */
    	$path = '../file/stream-file.xls';
        $argument = $input->getArgument('p');
        $option = $input->getOption('path');
		if(empty($option)){
			if(empty($argument)){
				$path = $argument;
			}
		}else{
			$path = $option;
		}
        
        $output->writeln($argument);
    }
}