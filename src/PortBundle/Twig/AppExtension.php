<?php
namespace PortBundle\Twig;

class AppExtension extends \Twig_Extension
{
	public function getFilters()
	{
		return array(
			new \Twig_SimpleFilter('app', array($this, 'priceFilter')),
		);
	}
	public function priceFilter($i)
	{
		return $i;
	}
}