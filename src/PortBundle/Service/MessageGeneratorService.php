<?php
namespace PortBundle\Service;


class MessageGeneratorService{
	private $mailer = 0;
	public function setMailer($mailer)
	{
		$this->mailer = $mailer;
		return $this;
	}
	public function getMailer()
	{
		return $this->mailer;
	}
}