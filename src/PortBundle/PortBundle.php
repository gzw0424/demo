<?php

namespace PortBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use PortBundle\DependencyInjection\RuleManagerCompilerPass;

class PortBundle extends Bundle
{
	public function build(ContainerBuilder $container)
	{
		parent::build($container);
		
		$container->addCompilerPass(new RuleManagerCompilerPass());
	}
}
