<?php

namespace FormBundle\Controller;

use BlogBundle\Entity\Article;
use FormBundle\Entity\Category;
use PortBundle\Service\MessageGeneratorService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Category controller.
 *
 */
class CategoryController extends Controller
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('FormBundle:Category')->findAll();

        return $this->render('FormBundle:Category:index.html.twig', array(
            'categories' => $categories,
        ));
    }

    /**
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $category = new Category();
        $form = $this->createForm('FormBundle\Form\CategoryType', $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $category->setParentId($this->categoryToId($category));
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('form_category_show', array('id' => $category->getId()));
        }

        return $this->render('FormBundle:Category:new.html.twig', array(
            'category' => $category,
            'form' => $form->createView(),
        ));
    }

    /**
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Category $category)
    {
        $category->setParentId($this->categoryToArticle($category)->getTitle());
        $deleteForm = $this->createDeleteForm($category);

        return $this->render('FormBundle:Category:show.html.twig', array(
            'category' => $category,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing category entity.
     * @param Request $request
     * @param Category $category
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Category $category)
    {
        $category->setParentId($this->categoryToArticle($category));
        $deleteForm = $this->createDeleteForm($category);
        $editForm = $this->createForm('FormBundle\Form\CategoryType', $category);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $category->setParentId($this->categoryToId($category));
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('form_category_edit', array('id' => $category->getId()));
        }

        return $this->render('FormBundle:Category:edit.html.twig', array(
            'category' => $category,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a category entity.
     *
     */
    public function deleteAction(Request $request, Category $category)
    {
        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush();
        }

        return $this->redirectToRoute('form_category_index');
    }

    /**
     * Creates a form to delete a category entity.
     *
     * @param Category $category The category entity
     *
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     *
     */
    private function createDeleteForm(Category $category)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('form_category_delete', array('id' => $category->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function categoryToArticle($category)
    {
        $parentId = $category->getParentId();
        $em = $this->getDoctrine()->getManager();
        return $em->getRepository('BlogBundle:Article')->find($parentId);
    }
    public function categoryToId($category)
    {
        return $category->getParentId()->getId();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function choiceAction()
    {
        return new Response('ss');
    }
}
