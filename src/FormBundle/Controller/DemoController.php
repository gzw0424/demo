<?php

namespace FormBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DemoController extends Controller
{
    /**
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('FormBundle:Demo:index.html.twig');
    }
}