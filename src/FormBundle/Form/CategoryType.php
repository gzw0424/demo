<?php

namespace FormBundle\Form;

use FormBundle\Form\Type\UploadfileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('parentId',EntityType::class,[
                'class' => 'BlogBundle:Article',
                'choice_label' => 'title',
                'label' => '父标题'
            ])->add('status',UploadfileType::class,[
                'label' => '银行附件',
                'attr' => [
                    'class' => 'layui-form-label layui-btn upload',
                    'style'=>'width:90px;',
                    'id'=>'upload'
                ],
            ])
        ;
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FormBundle\Entity\Category'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'formbundle_category';
    }


}
