<?php

namespace BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use BlogBundle\Entity\Article;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use BlogBundle\Entity\User;
use BlogBundle\Repository\UserRepository;

class ArticleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$builder
    	->add('title',TextType::class,[
    		'label' => '文章标题',
    		'attr' => [
    			'name' => 'title',
    			'lay-verify' => 'lay-verify',
    			'placeholder' => '文章的标题',
    			'autocomplete' => 'off'
    		]])
    	->add('content',TextareaType::class,[
    		'label' => '文章内容',
    		'attr' => [
    			'name' => 'content',
    			'lay-verify' => 'required',
    			'placeholder' => '这里是你要提交的文章正文',
    			'autocomplete' => 'off'
    		]])
    		->add('source',TextType::class,[
    		'label' => '来源',
    		'attr' => [
    			'name' => 'source',
    			'placeholder' => '文章的来源，自己编写的则应该为空',
    			'autocomplete' => 'off'
    		]])
    		->add('editor',EntityType::class,[
    			'class' => 'BlogBundle:User',
    			'query_builder' => function($repo){
    				return $repo->createQueryBuilder('u');
    			},
    			'choice_label' => 'username',
    			'choice_value' => function (User $entity = null) {
    				return $entity ? $entity->getId() : '';
    			},
    		]);
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BlogBundle\Entity\Article',
        	'editor' => '2'
        ));
    }
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'blogbundle_article';
    }
}