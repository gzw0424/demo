<?php
namespace BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use BlogBundle\Entity\User;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('username',TextType::class,[
			'label' => '用户名：',
			'attr' => [
				'name' => 'username',
				'lay-verify' => 'required|phone|number',
				'placeholder' => '请输入手机号',
				'autocomplete' => 'off',
			]])
		->add('password',PasswordType::class,[
			'label' => '密码：',
			'attr' => [
				'name' => 'password',
				'lay-verify' => 'required',
				'placeholder' => '请输入密码',
				'autocomplete' => 'off',
			]])
		;
		$builder->add('submit', SubmitType::class, [
			'label' => '提交'	,
			'attr' => [
				'class' => 'layui-input',
			]
		]);
	}
	public function configureOptions(OptionsResolver $resolver){
		$resolver->setDefaults(array(
			'data_class' => User::class,
		));
	}
	public function getBlockPrefix()
	{
		return 'blogbundle_user';
	}
}