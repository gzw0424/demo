<?php
namespace ArchiveBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use BaseBundle\Entity\LevelInterface;

/**
 * CorrectPersonnel
 *
 * @ORM\Table(name="correct_personnel",options={"comment":"社区矫正人员基本信息表"})
 * @ORM\Entity(repositoryClass="BlogBundle\Repository\CorrectPersonnelRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *      fields={"sQJZRYBH","sQJZRYBH"},
 *      message="社区矫正人员编号{{ value }}已存在"
 * )
 */
class CorrectPersonnel implements LevelInterface
{

    /**
     *
     * @var int
     * @ORM\Column(name="ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var string
     * @ORM\Column(name="SQJZRYBH", type="string", length=20,options={"comment":"社区矫正人员编号"})
     * @Assert\NotBlank(message="社区矫正人员编号不能为空")
     */
    private $sQJZRYBH;

    /**
     *
     * @var string
     * @ORM\Column(name="JZJGBM", type="string", length=18,options={"comment":"矫正机构编码"})
     * @Assert\NotBlank(message="矫正机构编码不能为空")
     */
    private $jZJGBM;

    /**
     *
     * @var string
     * @ORM\Column(name="SFDCPG", type="string", length=2,options={"comment":"是否调查评估"})
     * @Assert\NotBlank(message="是否调查评估不能为空")
     */
    private $sFDCPG;

    /**
     *
     * @var string
     * @ORM\Column(name="DCPGYJ", type="string",nullable=true,length=2,options={"comment":"调查评估意见"})
     */
    private $dCPGYJ;

    /**
     *
     * @var string
     * @ORM\Column(name="DCYJCXQK", type="string",nullable=true, length=2,options={"comment":"调查意见采信情况"})
     */
    private $dCYJCXQK;

    /**
     *
     * @var string
     * @ORM\Column(name="JZLB", type="string", length=2,options={"comment":"矫正类别"})
     * @Assert\NotBlank(message="矫正类别不能为空")
     */
    private $jZLB;

    /**
     *
     * @var string
     * @ORM\Column(name="SFCN", type="string", length=2,options={"comment":"是否成年"})
     * @Assert\NotBlank(message="是否成年不能为空")
     */
    private $sFCN;

    /**
     *
     * @var string
     * @ORM\Column(name="WCN", type="string", length=2, nullable=true,options={"comment":"未成年"})
     */
    private $wCN;

    /**
     *
     * @var string
     * @ORM\Column(name="XM", type="string", length=20,options={"comment":"姓名"})
     * @Assert\NotBlank(message="姓名不能为空")
     */
    private $xM;

    /**
     *
     * @var string @ORM\Column(name="CYM", type="string", length=20, nullable=true,options={"comment":"曾用名"})
     */
    private $cYM;

    /**
     *
     * @var string
     * @ORM\Column(name="XB", type="string", length=2,options={"comment":"性别"})
     * @Assert\NotBlank(message="性别不能为空")
     */
    private $xB;

    /**
     *
     * @var string
     * @ORM\Column(name="MZ", type="string", length=20,options={"comment":"民族"})
     * @Assert\NotBlank(message="民族不能为空")
     */
    private $mZ;

    /**
     *
     * @var string
     * @ORM\Column(name="SFZH", type="string", length=18,options={"comment":"身份证号"})
     * @Assert\NotBlank(message="身份证号不能为空")
     */
    private $sFZH;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CSRQ", type="datetime",options={"comment":"出生日期：格式（Y-m-d）"})
     * @Assert\NotBlank(message="出生日期不能为空")
     */
    private $cSRQ;

    /**
     *
     * @var string
     * @ORM\Column(name="YWGATSFZ", type="string", length=2,options={"comment":"有无港澳台身份证"})
     * @Assert\NotBlank(message="有无港澳台身份证不能为空")
     */
    private $yWGATSFZ;

    /**
     *
     * @var bool
     * @ORM\Column(name="GATSFZLX", type="smallint", nullable=true,options={"comment":"港澳台身份证类型"})
     */
    private $gATSFZLX;

    /**
     *
     * @var string
     * @ORM\Column(name="GATSFZHM", type="string", length=20, nullable=true,options={"comment":"港澳台身份证号码"})
     */
    private $gATSFZHM;

    /**
     *
     * @var bool
     * @ORM\Column(name="YWHZ", type="smallint",options={"comment":"有无护照"})
     * @Assert\NotBlank(message="有无护照不能为空")
     */
    private $yWHZ;

    /**
     *
     * @var string
     * @ORM\Column(name="HZHM", type="string", length=20, nullable=true,options={"comment":"护照号码"})
     */
    private $hZHM;

    /**
     *
     * @var bool
     * @ORM\Column(name="HZBCZT", type="smallint", nullable=true,options={"comment":"护照保存状态"})
     */
    private $hZBCZT;

    /**
     *
     * @var bool
     * @ORM\Column(name="YWGATTXZ", type="smallint",options={"comment":"有无港澳台通行证"})
     * @Assert\NotBlank(message="有无港澳台通行证不能为空")
     */
    private $yWGATTXZ;

    /**
     *
     * @var bool
     * @ORM\Column(name="GATTXZLX", type="smallint", nullable=true,options={"comment":"港澳台通行证类型"})
     */
    private $gATTXZLX;

    /**
     *
     * @var string
     * @ORM\Column(name="GATTXZHM", type="string", length=20, nullable=true,options={"comment":"港澳台通行证号码"})
     */
    private $gATTXZHM;

    /**
     *
     * @var bool @ORM\Column(name="GATTXZBCZT", type="smallint", nullable=true,options={"comment":"港澳台通行证保存状态"})
     */
    private $gATTXZBCZT;

    /**
     *
     * @var bool
     * @ORM\Column(name="YWGAJMWLNDTXZ", type="smallint",options={"comment":"有无港澳居民往来内地通行证"})
     * @Assert\NotBlank(message="有无港澳居民往来内地通行证不能为空")
     */
    private $yWGAJMWLNDTXZ;

    /**
     *
     * @var string
     * @ORM\Column(name="GAJMWLNDTXZ", type="string", length=20, nullable=true,options={"comment":"港澳居民往来内地通行证号码"})
     */
    private $gAJMWLNDTXZ;

    /**
     *
     * @var bool
     * @ORM\Column(name="GAJMWLNDTXZBCZT", type="smallint", nullable=true,options={"comment":"港澳居民往来内地通行证保存状态"})
     */
    private $gAJMWLNDTXZBCZT;

    /**
     *
     * @var bool
     * @ORM\Column(name="YWTBZ", type="smallint",options={"comment":"有无台胞证"})
     * @Assert\NotBlank(message="有无台胞证不能为空")
     */
    private $yWTBZ;

    /**
     *
     * @var string
     * @ORM\Column(name="TBZHM", type="string", length=20, nullable=true,options={"comment":"台胞证号码"})
     */
    private $tBZHM;

    /**
     *
     * @var bool
     * @ORM\Column(name="TBZBCZT", type="smallint", nullable=true,options={"comment":"台胞证保存状态"})
     */
    private $tBZBCZT;

    /**
     *
     * @var bool @ORM\Column(name="ZYJWZXRYSTZK", type="smallint", nullable=true,options={"comment":"暂予监外执行人员身体状况"})
     */
    private $zYJWZXRYSTZK;

    /**
     *
     * @var string
     * @ORM\Column(name="ZHJZYY", type="string", length=50, nullable=true,options={"comment":"最后就诊医院"})
     */
    private $zHJZYY;

    /**
     *
     * @var bool
     * @ORM\Column(name="SFYJSB", type="smallint",options={"comment":"是否有神经病"})
     * @Assert\NotBlank(message="是否有神经病不能为空")
     */
    private $sFYJSB;

    /**
     *
     * @var string @ORM\Column(name="JDJG", type="string", length=50, nullable=true,options={"comment":"鉴定机构"})
     */
    private $jDJG;

    /**
     *
     * @var bool
     * @ORM\Column(name="SFYCRB", type="smallint",options={"comment":"是否有传染病"})
     * @Assert\NotBlank(message="是否有传染病不能为空")
     */
    private $sFYCRB;

    /**
     *
     * @var string
     * @ORM\Column(name="JTCRB", type="string", length=100, nullable=true,options={"comment":"具体传染病"})
     */
    private $jTCRB;

    /**
     *
     * @var string
     * @ORM\Column(name="WHCD", type="string", length=20,options={"comment":"文化程度"})
     * @Assert\NotBlank(message="文化程度不能为空")
     */
    private $wHCD;

    /**
     *
     * @var string @ORM\Column(name="HYZK", type="string", length=20,options={"comment":"婚姻状况"})
     *      @Assert\NotBlank(message="婚姻状况不能为空")
     */
    private $hYZK;

    /**
     *
     * @var string
     * @ORM\Column(name="PQZY", type="string", length=50,options={"comment":"捕前职业"})
     * @Assert\NotBlank(message="捕前职业不能为空")
     */
    private $pQZY;

    /**
     *
     * @var string
     * @ORM\Column(name="JYJXQK", type="string", length=20,options={"comment":"就业就学情况"})
     * @Assert\NotBlank(message="就业就学情况不能为空")
     */
    private $jYJXQK;

    /**
     *
     * @var string
     * @ORM\Column(name="XZZMM", type="string", length=20,options={"comment":"现政治面貌"})
     * @Assert\NotBlank(message="现政治面貌不能为空")
     */
    private $xZZMM;

    /**
     *
     * @var string
     * @ORM\Column(name="YZZMM", type="string", length=20,options={"comment":"原政治面貌"})
     * @Assert\NotBlank(message="原政治面貌不能为空")
     */
    private $yZZMM;

    /**
     *
     * @var string
     * @ORM\Column(name="YGZDW", type="string", length=100, nullable=true,options={"comment":"原工作单位"})
     */
    private $yGZDW;

    /**
     *
     * @var string
     * @ORM\Column(name="XGZDW", type="string", length=100, nullable=true,options={"comment":"现工作单位"})
     */
    private $xGZDW;

    /**
     *
     * @var string
     * @ORM\Column(name="DWLXDH", type="string", length=40, nullable=true,options={"comment":"单位联系电话"})
     */
    private $dWLXDH;

    /**
     *
     * @var string
     * @ORM\Column(name="GRLXDH", type="string", length=18,options={"comment":"个人联系电话"})
     * @Assert\NotBlank(message="个人联系电话不能为空")
     */
    private $gRLXDH;

    /**
     *
     * @var string
     * @ORM\Column(name="GUOJIA", type="string", length=3,options={"comment":"国家"})
     * @Assert\NotBlank(message="国家不能为空")
     */
    private $gUOJIA;

    /**
     *
     * @var string
     * @ORM\Column(name="GJ", type="string", length=2,options={"comment":"国籍"})
     * @Assert\NotBlank(message="国籍不能为空")
     */
    private $gJ;

    /**
     *
     * @var bool
     * @ORM\Column(name="YXJTCYJZYSHGX", type="smallint",options={"comment":"有无家庭成员及主要社会关系"})
     * @Assert\NotBlank(message="有无家庭成员及主要社会关系不能为空")
     */
    private $yXJTCYJZYSHGX;

    /**
     *
     * @var string
     * @ORM\Column(name="ZP", type="string", length=255,nullable=true,options={"comment":"照片"})
     */
    private $zP;

    /**
     *
     * @var bool
     * @ORM\Column(name="HJDSFYJZDXT", type="smallint",options={"comment":"户籍地是否与居住地相同"})
     * @Assert\NotBlank(message="户籍地是否与居住地相同不能为空")
     */
    private $hJDSFYJZDXT;

    /**
     *
     * @var string
     * @ORM\Column(name="GDJZDSZS", type="string", length=20,options={"comment":"固定居住地所在省（区、市）"})
     * @Assert\NotBlank(message="固定居住地所在省（区、市）不能为空")
     */
    private $gDJZDSZS;

    /**
     *
     * @var string
     * @ORM\Column(name="GDJZDSZDS", type="string", length=20,options={"comment":"固定居住地所在地（市、州）"})
     * @Assert\NotBlank(message="固定居住地所在地（市、州）不能为空")
     */
    private $gDJZDSZDS;

    /**
     *
     * @var string
     * @ORM\Column(name="GDJZDSZXQ", type="string", length=20,options={"comment":"固定居住地所在县（市、区）"})
     * @Assert\NotBlank(message="固定居住地所在县（市、区）不能为空")
     */
    private $gDJZDSZXQ;

    /**
     *
     * @var string
     * @ORM\Column(name="GDJZD", type="string", length=20,options={"comment":"固定居住地（乡镇、街道）"})
     * @Assert\NotBlank(message="固定居住地（乡镇、街道）不能为空")
     */
    private $gDJZD;

    /**
     *
     * @var string
     * @ORM\Column(name="GDJZDMX", type="string", length=100,options={"comment":"固定居住地明细"})
     * @Assert\NotBlank(message="固定居住地明细不能为空")
     */
    private $gDJZDMX;

    /**
     *
     * @var string
     * @ORM\Column(name="HJSZS", type="string", length=20,options={"comment":"户籍所在省（区、市）"})
     * @Assert\NotBlank(message="户籍所在省（区、市）不能为空")
     */
    private $hJSZS;

    /**
     *
     * @var string
     * @ORM\Column(name="HJSZDS", type="string", length=20,options={"comment":"户籍所在地（市、州）"})
     * @Assert\NotBlank(message="户籍所在地（市、州）不能为空")
     */
    private $hJSZDS;

    /**
     *
     * @var string
     * @ORM\Column(name="HJSZXQ", type="string", length=20,options={"comment":"户籍所在县（市、区）"})
     * @Assert\NotBlank(message="户籍所在县（市、区）不能为空")
     */
    private $hJSZXQ;

    /**
     *
     * @var string
     * @ORM\Column(name="HJSZD", type="string", length=20,options={"comment":"户籍所在地（乡镇、街道）"})
     * @Assert\NotBlank(message="户籍所在地（乡镇、街道）不能为空")
     */
    private $hJSZD;

    /**
     *
     * @var string
     * @ORM\Column(name="HJSZDMX", type="string", length=100,options={"comment":"户籍所在地明细"})
     * @Assert\NotBlank(message="户籍所在地明细不能为空")
     */
    private $hJSZDMX;

    /**
     *
     * @var bool
     * @ORM\Column(name="SFSWRY", type="smallint",options={"comment":"是否三无人员"})
     * @Assert\NotBlank(message="是否三无人员不能为空")
     */
    private $sFSWRY;

    /**
     *
     * @var bool
     * @ORM\Column(name="SQJZJDJG", type="smallint",options={"comment":"社区纠正决定机关"})
     * @Assert\NotBlank(message="社区纠正决定机关不能为空")
     */
    private $sQJZJDJG;

    /**
     *
     * @var string
     * @ORM\Column(name="SQJZJDJGMC", type="string", length=50,options={"comment":"社区纠正决定机关名称"})
     * @Assert\NotBlank(message="社区纠正决定机关名称不能为空")
     */
    private $sQJZJDJGMC;

    /**
     *
     * @var string
     * @ORM\Column(name="ZXTZSWH", type="string", length=100,options={"comment":"执行通知书文号"})
     * @Assert\NotBlank(message="执行通知书文号不能为空")
     */
    private $zXTZSWH;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ZXTZSRQ", type="datetime",options={"comment":"执行通知书日期：格式（Y-m-d）"})
     * @Assert\NotBlank(message="执行通知书日期不能为空")
     */
    private $zXTZSRQ;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="JFZXRQ", type="datetime",options={"comment":"交付执行日期：格式（Y-m-d）"})
     * @Assert\NotBlank(message="交付执行日期不能为空")
     */
    private $jFZXRQ;

    /**
     *
     * @var bool
     * @ORM\Column(name="YJZFJG", type="smallint", nullable=true,options={"comment":"移交罪犯机关"})
     */
    private $yJZFJG;

    /**
     *
     * @var string
     * @ORM\Column(name="YJZFJGMC", type="string", length=50, nullable=true,options={"comment":"移交罪犯机关名称"})
     */
    private $yJZFJGMC;

    /**
     *
     * @var bool
     * @ORM\Column(name="SFYQK", type="smallint",options={"comment":"是否有前科"})
     * @Assert\NotBlank(message="是否有前科不能为空")
     */
    private $sFYQK;

    /**
     *
     * @var bool
     * @ORM\Column(name="SFLF", type="smallint",options={"comment":"是否累犯"})
     * @Assert\NotBlank(message="是否累犯不能为空")
     */
    private $sFLF;

    /**
     *
     * @var bool
     * @ORM\Column(name="QKLX", type="string",length=6, nullable=true,options={"comment":"前科类型"})
     */
    private $qKLX;

    /**
     *
     * @var string
     * @ORM\Column(name="ZYFZSS", type="string", length=500,options={"comment":"主要犯罪事实"})
     * @Assert\NotBlank(message="主要犯罪事实不能为空")
     */
    private $zYFZSS;

    /**
     *
     * @var string
     * @ORM\Column(name="SQJZQX", type="string", length=20,options={"comment":"社区矫正期限"})
     * @Assert\NotBlank(message="社区矫正期限不能为空")
     */
    private $sQJZQX;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="SQJZKSRQ", type="datetime",options={"comment":"社区矫正开始时间"})
     * @Assert\NotBlank(message="社区矫正开始时间不能为空")
     */
    private $sQJZKSRQ;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="SQJZJSRQ", type="datetime",options={"comment":"社区矫正结束时间"})
     * @Assert\NotBlank(message="社区矫正结束时间不能为空")
     */
    private $sQJZJSRQ;

    /**
     *
     * @var bool
     * @ORM\Column(name="FZLX", type="string",length=50,options={"comment":"犯罪类型"})
     * @Assert\NotBlank(message="犯罪类型不能为空")
     */
    private $fZLX;

    /**
     *
     * @var string
     * @ORM\Column(name="JTZM", type="string", length=50,options={"comment":"具体罪名"})
     * @Assert\NotBlank(message="具体罪名不能为空")
     */
    private $jTZM;

    /**
     *
     * @var bool
     * @ORM\Column(name="GZQX", type="smallint", nullable=true,options={"comment":"管制期限"})
     */
    private $gZQX;

    /**
     *
     * @var bool
     * @ORM\Column(name="HXKYQX", type="smallint", nullable=true,options={"comment":"缓刑考验期限"})
     */
    private $hXKYQX;

    /**
     *
     * @var bool
     * @ORM\Column(name="SFSZBF", type="smallint",options={"comment":"是否数罪并罚"})
     * @Assert\NotBlank(message="是否数罪并罚不能为空")
     */
    private $sFSZBF;

    /**
     *
     * @var string
     * @ORM\Column(name="YPXF", type="string", length=2,options={"comment":"原判刑罚"})
     * @Assert\NotBlank(message="原判刑罚不能为空")
     */
    private $yPXF;

    /**
     *
     * @var string
     * @ORM\Column(name="YPXQ", type="string", length=200,options={"comment":"原判刑期"})
     * @Assert\NotBlank(message="原判刑期不能为空")
     */
    private $yPXQ;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="YPXQKSRQ", type="datetime",options={"comment":"原判刑期开始日期"})
     * @Assert\NotBlank(message="原判刑期开始日期不能为空")
     */
    private $yPXQKSRQ;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="YPXQJSRQ", type="datetime",options={"comment":"原判刑期结束日期"})
     * @Assert\NotBlank(message="原判刑期结束日期不能为空")
     */
    private $yPXQJSRQ;

    /**
     *
     * @var bool
     * @ORM\Column(name="YQTXQX", type="smallint", nullable=true,options={"comment":"有期徒刑期限"})
     */
    private $yQTXQX;

    /**
     *
     * @var string
     * @ORM\Column(name="FJX", type="string", length=2,options={"comment":"附加刑"})
     * @Assert\NotBlank(message="附加刑不能为空")
     */
    private $fJX;

    /**
     *
     * @var string
     * @ORM\Column(name="SFWD", type="string", length=10,options={"comment":"是否“五独”"})
     * @Assert\NotBlank(message="是否“五独”不能为空")
     */
    private $sFWD;

    /**
     *
     * @var string
     * @ORM\Column(name="SFWS", type="string", length=10,options={"comment":"是否“五涉”"})
     * @Assert\NotBlank(message="是否“五涉”不能为空")
     */
    private $sFWS;

    /**
     *
     * @var string
     * @ORM\Column(name="SFYSS", type="string",length=30,options={"comment":"是否有“四史”"})
     * @Assert\NotBlank(message="是否有“四史”不能为空")
     */
    private $sFYSS;

    /**
     *
     * @var bool
     * @ORM\Column(name="SFBXGJZL", type="smallint",options={"comment":"是否被宣告禁止令"})
     * @Assert\NotBlank(message="是否被宣告禁止令不能为空")
     */
    private $sFBXGJZL;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="SQJZRYJSRQ", type="datetime",length=20,options={"comment":"社区矫正人员接受日期"})
     * @Assert\NotBlank(message="社区矫正人员接受日期不能为空")
     */
    private $sQJZRYJSRQ;

    /**
     *
     * @var bool
     * @ORM\Column(name="SQJZRYJSFS", type="smallint",options={"comment":"社区矫正人员接受方式"})
     * @Assert\NotBlank(message="社区矫正人员接受方式不能为空")
     */
    private $sQJZRYJSFS;

    /**
     *
     * @var string @ORM\Column(name="BDQK", type="string",length=2,options={"comment":"报道情况"})
     * @Assert\NotBlank(message="报道情况不能为空")
     */
    private $bDQK;

    /**
     *
     * @var string
     * @ORM\Column(name="WASBDQKSM", type="string", nullable=true, length=200,options={"comment":"未按时报道情况说明"})
     */
    private $wASBDQKSM;

    /**
     *
     * @var bool
     * @ORM\Column(name="SFJLJZXZ", type="smallint",options={"comment":"是否建立矫正小组"})
     * @Assert\NotBlank(message="是否建立矫正小组不能为空")
     */
    private $sFJLJZXZ;

    /**
     *
     * @var string
     * @ORM\Column(name="JZXZRYZCQK", type="string",length=30,nullable=true,options={"comment":"矫正小组人员组成情况"})
     */
    private $jZXZRYZCQK;

    /**
     *
     * @var bool
     * @ORM\Column(name="SFCYDZDWGL", type="smallint",options={"comment":"是否采用电子定位管理"})
     * @Assert\NotBlank(message="是否采用电子定位管理不能为空")
     */
    private $sFCYDZDWGL;

    /**
     *
     * @var bool
     * @ORM\Column(name="DZDWFS", type="smallint",nullable=true,options={"comment":"电子定位方式"})
     */
    private $dZDWFS;

    /**
     *
     * @var string
     * @ORM\Column(name="DWHM", type="string",length=20,nullable=true,options={"comment":"定位号码"})
     */
    private $dWHM;

    /**
     *
     * @var bool
     * @ORM\Column(name="SFTG", type="smallint",options={"comment":"是否脱管"})
     * @Assert\NotBlank(message="是否脱管不能为空")
     */
    private $sFTG;

    /**
     *
     * @var bool
     * @ORM\Column(name="JCQK", type="smallint",options={"comment":"奖惩情况"})
     * @Assert\NotBlank(message="奖惩情况不能为空")
     */
    private $jCQK;

    /**
     *
     * @var smallint
     * @ORM\Column(name="SFKSZR", type="smallint",nullable=true,options={"comment":"是否跨省转入","default":0})
     */
    private $sFKSZR;

    /**
     *
     * @var string
     * @ORM\Column(name="BZ", type="string",length=500,nullable=true,options={"comment":"备注"})
     */
    private $bZ;

    /**
     *
     * @var string
     * @ORM\Column(name="JZJGSZSFT", type="string", length=18,options={"comment":"矫正人员所属司法厅"})
     * @Assert\NotBlank(message="矫正人员所属司法厅不能为空")
     */
    private $jZJGSZSFT;

    /**
     *
     * @var string
     * @ORM\Column(name="JZJGSZDSSFJ", type="string", length=18,options={"comment":"矫正人员所属地市司法局"})
     * @Assert\NotBlank(message="矫正人员所属地市司法局不能为空")
     */
    private $jZJGSZDSSFJ;

    /**
     *
     * @var string
     * @ORM\Column(name="JZJGSZDSSFJMC", type="string", length=20,options={"comment":"矫正人员所属地市司法局名称"})
     */
    private $jZJGSZDSSFJMC;

    /**
     *
     * @var string
     * @ORM\Column(name="JZJGSZQXSFJ", type="string", length=18,options={"comment":"矫正人员所属区县司法局"})
     * @Assert\NotBlank(message="矫正人员所属区县司法局不能为空")
     */
    private $jZJGSZQXSFJ;

    /**
     *
     * @var string
     * @ORM\Column(name="JZJGSZQXSFJMC", type="string", length=18,options={"comment":"矫正人员所属区县司法局名称"})
     */
    private $jZJGSZQXSFJMC;

    /**
     *
     * @var string
     * @ORM\Column(name="JZJGSZSFS", type="string", length=18,options={"comment":"矫正人员所属司法局"})
     * @Assert\NotBlank(message="矫正人员所属司法局不能为空")
     */
    private $jZJGSZSFS;

    /**
     *
     * @var string
     * @ORM\Column(name="JZJGSZSFSMC", type="string", length=18,options={"comment":"矫正人员所属司法所名称"})
     */
    private $jZJGSZSFSMC;

    /**
     * @var string
     *
     * @ORM\Column(name="IP", type="string", length=20, nullable=true,options={"comment":"IP地址"})
     */
    private $iP;

    /**
     *
     * @var smallint
     * @ORM\Column(name="SUBMITSTATE", type="smallint", length=2, nullable=true,options={"comment":"提交状态","default":"0"})
     */
    private $sUBMITSTATE;

    /**
     *
     * @var string
     * @ORM\Column(name="SUBMITTIME", type="string", length=10, nullable=true,options={"comment":"提交时间（时间戳）"})
     */
    private $sUBMITTIME;

    /**
     *
     * @var string
     * @ORM\Column(name="SUBMITRESULT", type="string", length=255, nullable=true,options={"comment":"提交结果反馈"})
     */
    private $sUBMITRESULT;
    /**
     * @var int
     *
     * @ORM\Column(name="XSTBZT", type="smallint",options={"comment":"xunsearch同步状态：0新增,待同步；1正常，已同步；2已修改，待同步","default": 0})
     */
    private $xSTBZT;
    
    /**
     * @var int
     * 
     * @ORM\Column(name="JZZT", type="smallint",options={"comment":"矫正状态 0:未矫 ;1:再矫"})
     */
    private $jZZT;

    /**
     * @var string
     *
     * @ORM\Column(name="GLJB", type="string",length=2,options={"comment":"管理级别  01：宽松  02：普通 03：严格 "})
     */
    private $gLJB;
    /**
     *
     * @var \DateTime
     * @ORM\Column(name="CREATEAT", type="datetimetz", nullable=true,options={"comment":"创建时间"})
     */
    private $cREATEAT;

    /**
     *
     * @var \DateTime
     * @ORM\Column(name="UPDATEAT", type="datetimetz", nullable=true,options={"comment":"更新时间"})
     */
    private $uPDATEAT;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sQJZRYBH
     *
     * @param string $sQJZRYBH
     *
     * @return CorrectPersonnel
     */
    public function setSQJZRYBH($sQJZRYBH)
    {
        $this->sQJZRYBH = $sQJZRYBH;

        return $this;
    }

    /**
     * Get sQJZRYBH
     *
     * @return string
     */
    public function getSQJZRYBH()
    {
        return $this->sQJZRYBH;
    }

    /**
     * Set jZJGBM
     *
     * @param string $jZJGBM
     *
     * @return CorrectPersonnel
     */
    public function setJZJGBM($jZJGBM)
    {
        $this->jZJGBM = $jZJGBM;

        return $this;
    }

    /**
     * Get jZJGBM
     *
     * @return string
     */
    public function getJZJGBM()
    {
        return $this->jZJGBM;
    }

    /**
     * Set sFDCPG
     *
     * @param string $sFDCPG
     *
     * @return CorrectPersonnel
     */
    public function setSFDCPG($sFDCPG)
    {
        $this->sFDCPG = $sFDCPG;

        return $this;
    }

    /**
     * Get sFDCPG
     *
     * @return string
     */
    public function getSFDCPG()
    {
        return $this->sFDCPG;
    }

    /**
     * Set dCPGYJ
     *
     * @param string $dCPGYJ
     *
     * @return CorrectPersonnel
     */
    public function setDCPGYJ($dCPGYJ)
    {
        $this->dCPGYJ = $dCPGYJ;

        return $this;
    }

    /**
     * Get dCPGYJ
     *
     * @return string
     */
    public function getDCPGYJ()
    {
        return $this->dCPGYJ;
    }

    /**
     * Set dCYJCXQK
     *
     * @param string $dCYJCXQK
     *
     * @return CorrectPersonnel
     */
    public function setDCYJCXQK($dCYJCXQK)
    {
        $this->dCYJCXQK = $dCYJCXQK;

        return $this;
    }

    /**
     * Get dCYJCXQK
     *
     * @return string
     */
    public function getDCYJCXQK()
    {
        return $this->dCYJCXQK;
    }

    /**
     * Set jZLB
     *
     * @param string $jZLB
     *
     * @return CorrectPersonnel
     */
    public function setJZLB($jZLB)
    {
        $this->jZLB = $jZLB;

        return $this;
    }

    /**
     * Get jZLB
     *
     * @return string
     */
    public function getJZLB()
    {
        return $this->jZLB;
    }

    /**
     * Set sFCN
     *
     * @param string $sFCN
     *
     * @return CorrectPersonnel
     */
    public function setSFCN($sFCN)
    {
        $this->sFCN = $sFCN;

        return $this;
    }

    /**
     * Get sFCN
     *
     * @return string
     */
    public function getSFCN()
    {
        return $this->sFCN;
    }

    /**
     * Set wCN
     *
     * @param string $wCN
     *
     * @return CorrectPersonnel
     */
    public function setWCN($wCN)
    {
        $this->wCN = $wCN;

        return $this;
    }

    /**
     * Get wCN
     *
     * @return string
     */
    public function getWCN()
    {
        return $this->wCN;
    }

    /**
     * Set xM
     *
     * @param string $xM
     *
     * @return CorrectPersonnel
     */
    public function setXM($xM)
    {
        $this->xM = $xM;

        return $this;
    }

    /**
     * Get xM
     *
     * @return string
     */
    public function getXM()
    {
        return $this->xM;
    }

    /**
     * Set cYM
     *
     * @param string $cYM
     *
     * @return CorrectPersonnel
     */
    public function setCYM($cYM)
    {
        $this->cYM = $cYM;

        return $this;
    }

    /**
     * Get cYM
     *
     * @return string
     */
    public function getCYM()
    {
        return $this->cYM;
    }

    /**
     * Set xB
     *
     * @param string $xB
     *
     * @return CorrectPersonnel
     */
    public function setXB($xB)
    {
        $this->xB = $xB;

        return $this;
    }

    /**
     * Get xB
     *
     * @return string
     */
    public function getXB()
    {
        return $this->xB;
    }

    /**
     * Set mZ
     *
     * @param string $mZ
     *
     * @return CorrectPersonnel
     */
    public function setMZ($mZ)
    {
        $this->mZ = $mZ;

        return $this;
    }

    /**
     * Get mZ
     *
     * @return string
     */
    public function getMZ()
    {
        return $this->mZ;
    }

    /**
     * Set sFZH
     *
     * @param string $sFZH
     *
     * @return CorrectPersonnel
     */
    public function setSFZH($sFZH)
    {
        $this->sFZH = $sFZH;

        return $this;
    }

    /**
     * Get sFZH
     *
     * @return string
     */
    public function getSFZH()
    {
        return $this->sFZH;
    }

    /**
     * Set cSRQ
     *
     * @param string $cSRQ
     *
     * @return CorrectPersonnel
     */
    public function setCSRQ($cSRQ)
    {
        $this->cSRQ = $cSRQ;

        return $this;
    }

    /**
     * Get cSRQ
     *
     * @return string
     */
    public function getCSRQ()
    {
        return $this->cSRQ;
    }

    /**
     * Set yWGATSFZ
     *
     * @param string $yWGATSFZ
     *
     * @return CorrectPersonnel
     */
    public function setYWGATSFZ($yWGATSFZ)
    {
        $this->yWGATSFZ = $yWGATSFZ;

        return $this;
    }

    /**
     * Get yWGATSFZ
     *
     * @return string
     */
    public function getYWGATSFZ()
    {
        return $this->yWGATSFZ;
    }

    /**
     * Set gATSFZLX
     *
     * @param string $gATSFZLX
     *
     * @return CorrectPersonnel
     */
    public function setGATSFZLX($gATSFZLX)
    {
        $this->gATSFZLX = $gATSFZLX;

        return $this;
    }

    /**
     * Get gATSFZLX
     *
     * @return string
     */
    public function getGATSFZLX()
    {
        return $this->gATSFZLX;
    }

    /**
     * Set gATSFZHM
     *
     * @param string $gATSFZHM
     *
     * @return CorrectPersonnel
     */
    public function setGATSFZHM($gATSFZHM)
    {
        $this->gATSFZHM = $gATSFZHM;

        return $this;
    }

    /**
     * Get gATSFZHM
     *
     * @return string
     */
    public function getGATSFZHM()
    {
        return $this->gATSFZHM;
    }

    /**
     * Set yWHZ
     *
     * @param smallint $yWHZ
     *
     * @return CorrectPersonnel
     */
    public function setYWHZ($yWHZ)
    {
        $this->yWHZ = $yWHZ;

        return $this;
    }

    /**
     * Get yWHZ
     *
     * @return bool
     */
    public function getYWHZ()
    {
        return $this->yWHZ;
    }

    /**
     * Set hZHM
     *
     * @param string $hZHM
     *
     * @return CorrectPersonnel
     */
    public function setHZHM($hZHM)
    {
        $this->hZHM = $hZHM;

        return $this;
    }

    /**
     * Get hZHM
     *
     * @return string
     */
    public function getHZHM()
    {
        return $this->hZHM;
    }

    /**
     * Set hZBCZT
     *
     * @param smallint $hZBCZT
     *
     * @return CorrectPersonnel
     */
    public function setHZBCZT($hZBCZT)
    {
        $this->hZBCZT = $hZBCZT;

        return $this;
    }

    /**
     * Get hZBCZT
     *
     * @return bool
     */
    public function getHZBCZT()
    {
        return $this->hZBCZT;
    }

    /**
     * Set yWGATTXZ
     *
     * @param smallint $yWGATTXZ
     *
     * @return CorrectPersonnel
     */
    public function setYWGATTXZ($yWGATTXZ)
    {
        $this->yWGATTXZ = $yWGATTXZ;

        return $this;
    }

    /**
     * Get yWGATTXZ
     *
     * @return bool
     */
    public function getYWGATTXZ()
    {
        return $this->yWGATTXZ;
    }

    /**
     * Set gATTXZLX
     *
     * @param smallint $gATTXZLX
     *
     * @return CorrectPersonnel
     */
    public function setGATTXZLX($gATTXZLX)
    {
        $this->gATTXZLX = $gATTXZLX;

        return $this;
    }

    /**
     * Get gATTXZLX
     *
     * @return bool
     */
    public function getGATTXZLX()
    {
        return $this->gATTXZLX;
    }

    /**
     * Set gATTXZHM
     *
     * @param string $gATTXZHM
     *
     * @return CorrectPersonnel
     */
    public function setGATTXZHM($gATTXZHM)
    {
        $this->gATTXZHM = $gATTXZHM;

        return $this;
    }

    /**
     * Get gATTXZHM
     *
     * @return string
     */
    public function getGATTXZHM()
    {
        return $this->gATTXZHM;
    }

    /**
     * Set gATTXZBCZT
     *
     * @param smallint $gATTXZBCZT
     *
     * @return CorrectPersonnel
     */
    public function setGATTXZBCZT($gATTXZBCZT)
    {
        $this->gATTXZBCZT = $gATTXZBCZT;

        return $this;
    }

    /**
     * Get gATTXZBCZT
     *
     * @return bool
     */
    public function getGATTXZBCZT()
    {
        return $this->gATTXZBCZT;
    }

    /**
     * Set yWGAJMWLNDTXZ
     *
     * @param smallint $yWGAJMWLNDTXZ
     *
     * @return CorrectPersonnel
     */
    public function setYWGAJMWLNDTXZ($yWGAJMWLNDTXZ)
    {
        $this->yWGAJMWLNDTXZ = $yWGAJMWLNDTXZ;

        return $this;
    }

    /**
     * Get yWGAJMWLNDTXZ
     *
     * @return bool
     */
    public function getYWGAJMWLNDTXZ()
    {
        return $this->yWGAJMWLNDTXZ;
    }

    /**
     * Set gAJMWLNDTXZ
     *
     * @param string $gAJMWLNDTXZ
     *
     * @return CorrectPersonnel
     */
    public function setGAJMWLNDTXZ($gAJMWLNDTXZ)
    {
        $this->gAJMWLNDTXZ = $gAJMWLNDTXZ;

        return $this;
    }

    /**
     * Get gAJMWLNDTXZ
     *
     * @return string
     */
    public function getGAJMWLNDTXZ()
    {
        return $this->gAJMWLNDTXZ;
    }

    /**
     * Set gAJMWLNDTXZBCZT
     *
     * @param smallint $gAJMWLNDTXZBCZT
     *
     * @return CorrectPersonnel
     */
    public function setGAJMWLNDTXZBCZT($gAJMWLNDTXZBCZT)
    {
        $this->gAJMWLNDTXZBCZT = $gAJMWLNDTXZBCZT;

        return $this;
    }

    /**
     * Get gAJMWLNDTXZBCZT
     *
     * @return bool
     */
    public function getGAJMWLNDTXZBCZT()
    {
        return $this->gAJMWLNDTXZBCZT;
    }

    /**
     * Set yWTBZ
     *
     * @param smallint $yWTBZ
     *
     * @return CorrectPersonnel
     */
    public function setYWTBZ($yWTBZ)
    {
        $this->yWTBZ = $yWTBZ;

        return $this;
    }

    /**
     * Get yWTBZ
     *
     * @return bool
     */
    public function getYWTBZ()
    {
        return $this->yWTBZ;
    }

    /**
     * Set tBZHM
     *
     * @param string $tBZHM
     *
     * @return CorrectPersonnel
     */
    public function setTBZHM($tBZHM)
    {
        $this->tBZHM = $tBZHM;

        return $this;
    }

    /**
     * Get tBZHM
     *
     * @return string
     */
    public function getTBZHM()
    {
        return $this->tBZHM;
    }

    /**
     * Set tBZBCZT
     *
     * @param smallint $tBZBCZT
     *
     * @return CorrectPersonnel
     */
    public function setTBZBCZT($tBZBCZT)
    {
        $this->tBZBCZT = $tBZBCZT;

        return $this;
    }

    /**
     * Get tBZBCZT
     *
     * @return bool
     */
    public function getTBZBCZT()
    {
        return $this->tBZBCZT;
    }

    /**
     * Set zYJWZXRYSTZK
     *
     * @param string $zYJWZXRYSTZK
     *
     * @return CorrectPersonnel
     */
    public function setZYJWZXRYSTZK($zYJWZXRYSTZK)
    {
        $this->zYJWZXRYSTZK = $zYJWZXRYSTZK;

        return $this;
    }

    /**
     * Get zYJWZXRYSTZK
     *
     * @return string
     */
    public function getZYJWZXRYSTZK()
    {
        return $this->zYJWZXRYSTZK;
    }

    /**
     * Set zHJZYY
     *
     * @param string $zHJZYY
     *
     * @return CorrectPersonnel
     */
    public function setZHJZYY($zHJZYY)
    {
        $this->zHJZYY = $zHJZYY;

        return $this;
    }

    /**
     * Get zHJZYY
     *
     * @return string
     */
    public function getZHJZYY()
    {
        return $this->zHJZYY;
    }

    /**
     * Set sFYJSB
     *
     * @param smallint $sFYJSB
     *
     * @return CorrectPersonnel
     */
    public function setSFYJSB($sFYJSB)
    {
        $this->sFYJSB = $sFYJSB;

        return $this;
    }

    /**
     * Get sFYJSB
     *
     * @return bool
     */
    public function getSFYJSB()
    {
        return $this->sFYJSB;
    }

    /**
     * Set jDJG
     *
     * @param string $jDJG
     *
     * @return CorrectPersonnel
     */
    public function setJDJG($jDJG)
    {
        $this->jDJG = $jDJG;

        return $this;
    }

    /**
     * Get jDJG
     *
     * @return string
     */
    public function getJDJG()
    {
        return $this->jDJG;
    }

    /**
     * Set sFYCRB
     *
     * @param smallint $sFYCRB
     *
     * @return CorrectPersonnel
     */
    public function setSFYCRB($sFYCRB)
    {
        $this->sFYCRB = $sFYCRB;

        return $this;
    }

    /**
     * Get sFYCRB
     *
     * @return bool
     */
    public function getSFYCRB()
    {
        return $this->sFYCRB;
    }

    /**
     * Set jTCRB
     *
     * @param string $jTCRB
     *
     * @return CorrectPersonnel
     */
    public function setJTCRB($jTCRB)
    {
        $this->jTCRB = $jTCRB;

        return $this;
    }

    /**
     * Get jTCRB
     *
     * @return string
     */
    public function getJTCRB()
    {
        return $this->jTCRB;
    }

    /**
     * Set wHCD
     *
     * @param string $wHCD
     *
     * @return CorrectPersonnel
     */
    public function setWHCD($wHCD)
    {
        $this->wHCD = $wHCD;

        return $this;
    }

    /**
     * Get wHCD
     *
     * @return string
     */
    public function getWHCD()
    {
        return $this->wHCD;
    }

    /**
     * Set hYZK
     *
     * @param string $hYZK
     *
     * @return CorrectPersonnel
     */
    public function setHYZK($hYZK)
    {
        $this->hYZK = $hYZK;

        return $this;
    }

    /**
     * Get hYZK
     *
     * @return string
     */
    public function getHYZK()
    {
        return $this->hYZK;
    }

    /**
     * Set pQZY
     *
     * @param string $pQZY
     *
     * @return CorrectPersonnel
     */
    public function setPQZY($pQZY)
    {
        $this->pQZY = $pQZY;

        return $this;
    }

    /**
     * Get pQZY
     *
     * @return string
     */
    public function getPQZY()
    {
        return $this->pQZY;
    }

    /**
     * Set jYJXQK
     *
     * @param string $jYJXQK
     *
     * @return CorrectPersonnel
     */
    public function setJYJXQK($jYJXQK)
    {
        $this->jYJXQK = $jYJXQK;

        return $this;
    }

    /**
     * Get jYJXQK
     *
     * @return string
     */
    public function getJYJXQK()
    {
        return $this->jYJXQK;
    }

    /**
     * Set xZZMM
     *
     * @param string $xZZMM
     *
     * @return CorrectPersonnel
     */
    public function setXZZMM($xZZMM)
    {
        $this->xZZMM = $xZZMM;

        return $this;
    }

    /**
     * Get xZZMM
     *
     * @return string
     */
    public function getXZZMM()
    {
        return $this->xZZMM;
    }

    /**
     * Set yZZMM
     *
     * @param string $yZZMM
     *
     * @return CorrectPersonnel
     */
    public function setYZZMM($yZZMM)
    {
        $this->yZZMM = $yZZMM;

        return $this;
    }

    /**
     * Get yZZMM
     *
     * @return string
     */
    public function getYZZMM()
    {
        return $this->yZZMM;
    }

    /**
     * Set yGZDW
     *
     * @param string $yGZDW
     *
     * @return CorrectPersonnel
     */
    public function setYGZDW($yGZDW)
    {
        $this->yGZDW = $yGZDW;

        return $this;
    }

    /**
     * Get yGZDW
     *
     * @return string
     */
    public function getYGZDW()
    {
        return $this->yGZDW;
    }

    /**
     * Set xGZDW
     *
     * @param string $xGZDW
     *
     * @return CorrectPersonnel
     */
    public function setXGZDW($xGZDW)
    {
        $this->xGZDW = $xGZDW;

        return $this;
    }

    /**
     * Get xGZDW
     *
     * @return string
     */
    public function getXGZDW()
    {
        return $this->xGZDW;
    }

    /**
     * Set dWLXDH
     *
     * @param string $dWLXDH
     *
     * @return CorrectPersonnel
     */
    public function setDWLXDH($dWLXDH)
    {
        $this->dWLXDH = $dWLXDH;

        return $this;
    }

    /**
     * Get dWLXDH
     *
     * @return string
     */
    public function getDWLXDH()
    {
        return $this->dWLXDH;
    }

    /**
     * Set gRLXDH
     *
     * @param string $gRLXDH
     *
     * @return CorrectPersonnel
     */
    public function setGRLXDH($gRLXDH)
    {
        $this->gRLXDH = $gRLXDH;

        return $this;
    }

    /**
     * Get gRLXDH
     *
     * @return string
     */
    public function getGRLXDH()
    {
        return $this->gRLXDH;
    }

    /**
     * Set gJ
     *
     * @param string $gJ
     *
     * @return CorrectPersonnel
     */
    public function setGJ($gJ)
    {
        $this->gJ = $gJ;

        return $this;
    }

    /**
     * Get gJ
     *
     * @return string
     */
    public function getGJ()
    {
        return $this->gJ;
    }

    /**
     * Set yXJTCYJZYSHGX
     *
     * @param smallint $yXJTCYJZYSHGX
     *
     * @return CorrectPersonnel
     */
    public function setYXJTCYJZYSHGX($yXJTCYJZYSHGX)
    {
        $this->yXJTCYJZYSHGX = $yXJTCYJZYSHGX;

        return $this;
    }

    /**
     * Get yXJTCYJZYSHGX
     *
     * @return bool
     */
    public function getYXJTCYJZYSHGX()
    {
        return $this->yXJTCYJZYSHGX;
    }

    /**
     * Set zP
     *
     * @param string $zP
     *
     * @return CorrectPersonnel
     */
    public function setZP($zP)
    {
        $this->zP = $zP;

        return $this;
    }

    /**
     * Get zP
     *
     * @return string
     */
    public function getZP()
    {
        return $this->zP;
    }

    /**
     * Set hJDSFYJZDXT
     *
     * @param smallint $hJDSFYJZDXT
     *
     * @return CorrectPersonnel
     */
    public function setHJDSFYJZDXT($hJDSFYJZDXT)
    {
        $this->hJDSFYJZDXT = $hJDSFYJZDXT;

        return $this;
    }

    /**
     * Get hJDSFYJZDXT
     *
     * @return bool
     */
    public function getHJDSFYJZDXT()
    {
        return $this->hJDSFYJZDXT;
    }

    /**
     * Set gDJZDSZS
     *
     * @param string $gDJZDSZS
     *
     * @return CorrectPersonnel
     */
    public function setGDJZDSZS($gDJZDSZS)
    {
        $this->gDJZDSZS = $gDJZDSZS;

        return $this;
    }

    /**
     * Get gDJZDSZS
     *
     * @return string
     */
    public function getGDJZDSZS()
    {
        return $this->gDJZDSZS;
    }

    /**
     * Set gDJZDSZDS
     *
     * @param string $gDJZDSZDS
     *
     * @return CorrectPersonnel
     */
    public function setGDJZDSZDS($gDJZDSZDS)
    {
        $this->gDJZDSZDS = $gDJZDSZDS;

        return $this;
    }

    /**
     * Get gDJZDSZDS
     *
     * @return string
     */
    public function getGDJZDSZDS()
    {
        return $this->gDJZDSZDS;
    }

    /**
     * Set gDJZDSZXQ
     *
     * @param string $gDJZDSZXQ
     *
     * @return CorrectPersonnel
     */
    public function setGDJZDSZXQ($gDJZDSZXQ)
    {
        $this->gDJZDSZXQ = $gDJZDSZXQ;

        return $this;
    }

    /**
     * Get gDJZDSZXQ
     *
     * @return string
     */
    public function getGDJZDSZXQ()
    {
        return $this->gDJZDSZXQ;
    }

    /**
     * Set gDJZD
     *
     * @param string $gDJZD
     *
     * @return CorrectPersonnel
     */
    public function setGDJZD($gDJZD)
    {
        $this->gDJZD = $gDJZD;

        return $this;
    }

    /**
     * Get gDJZD
     *
     * @return string
     */
    public function getGDJZD()
    {
        return $this->gDJZD;
    }

    /**
     * Set gDJZDMX
     *
     * @param string $gDJZDMX
     *
     * @return CorrectPersonnel
     */
    public function setGDJZDMX($gDJZDMX)
    {
        $this->gDJZDMX = $gDJZDMX;

        return $this;
    }

    /**
     * Get gDJZDMX
     *
     * @return string
     */
    public function getGDJZDMX()
    {
        return $this->gDJZDMX;
    }

    /**
     * Set hJSZS
     *
     * @param string $hJSZS
     *
     * @return CorrectPersonnel
     */
    public function setHJSZS($hJSZS)
    {
        $this->hJSZS = $hJSZS;

        return $this;
    }

    /**
     * Get hJSZS
     *
     * @return string
     */
    public function getHJSZS()
    {
        return $this->hJSZS;
    }

    /**
     * Set hJSZDS
     *
     * @param string $hJSZDS
     *
     * @return CorrectPersonnel
     */
    public function setHJSZDS($hJSZDS)
    {
        $this->hJSZDS = $hJSZDS;

        return $this;
    }

    /**
     * Get hJSZDS
     *
     * @return string
     */
    public function getHJSZDS()
    {
        return $this->hJSZDS;
    }

    /**
     * Set hJSZXQ
     *
     * @param string $hJSZXQ
     *
     * @return CorrectPersonnel
     */
    public function setHJSZXQ($hJSZXQ)
    {
        $this->hJSZXQ = $hJSZXQ;

        return $this;
    }

    /**
     * Get hJSZXQ
     *
     * @return string
     */
    public function getHJSZXQ()
    {
        return $this->hJSZXQ;
    }

    /**
     * Set hJSZD
     *
     * @param string $hJSZD
     *
     * @return CorrectPersonnel
     */
    public function setHJSZD($hJSZD)
    {
        $this->hJSZD = $hJSZD;

        return $this;
    }

    /**
     * Get hJSZD
     *
     * @return string
     */
    public function getHJSZD()
    {
        return $this->hJSZD;
    }

    /**
     * Set hJSZDMX
     *
     * @param string $hJSZDMX
     *
     * @return CorrectPersonnel
     */
    public function setHJSZDMX($hJSZDMX)
    {
        $this->hJSZDMX = $hJSZDMX;

        return $this;
    }

    /**
     * Get hJSZDMX
     *
     * @return string
     */
    public function getHJSZDMX()
    {
        return $this->hJSZDMX;
    }

    /**
     * Set sFSWRY
     *
     * @param smallint $sFSWRY
     *
     * @return CorrectPersonnel
     */
    public function setSFSWRY($sFSWRY)
    {
        $this->sFSWRY = $sFSWRY;

        return $this;
    }

    /**
     * Get sFSWRY
     *
     * @return bool
     */
    public function getSFSWRY()
    {
        return $this->sFSWRY;
    }

    /**
     * Set sQJZJDJG
     *
     * @param smallint $sQJZJDJG
     *
     * @return CorrectPersonnel
     */
    public function setSQJZJDJG($sQJZJDJG)
    {
        $this->sQJZJDJG = $sQJZJDJG;

        return $this;
    }

    /**
     * Get sQJZJDJG
     *
     * @return bool
     */
    public function getSQJZJDJG()
    {
        return $this->sQJZJDJG;
    }

    /**
     * Set sQJZJDJGMC
     *
     * @param string $sQJZJDJGMC
     *
     * @return CorrectPersonnel
     */
    public function setSQJZJDJGMC($sQJZJDJGMC)
    {
        $this->sQJZJDJGMC = $sQJZJDJGMC;

        return $this;
    }

    /**
     * Get sQJZJDJGMC
     *
     * @return string
     */
    public function getSQJZJDJGMC()
    {
        return $this->sQJZJDJGMC;
    }

    /**
     * Set zXTZSWH
     *
     * @param string $zXTZSWH
     *
     * @return CorrectPersonnel
     */
    public function setZXTZSWH($zXTZSWH)
    {
        $this->zXTZSWH = $zXTZSWH;

        return $this;
    }

    /**
     * Get zXTZSWH
     *
     * @return string
     */
    public function getZXTZSWH()
    {
        return $this->zXTZSWH;
    }

    /**
     * Set zXTZSRQ
     *
     * @param string $zXTZSRQ
     *
     * @return CorrectPersonnel
     */
    public function setZXTZSRQ($zXTZSRQ)
    {
        $this->zXTZSRQ = $zXTZSRQ;

        return $this;
    }

    /**
     * Get zXTZSRQ
     *
     * @return string
     */
    public function getZXTZSRQ()
    {
        return $this->zXTZSRQ;
    }

    /**
     * Set jFZXRQ
     *
     * @param string $jFZXRQ
     *
     * @return CorrectPersonnel
     */
    public function setJFZXRQ($jFZXRQ)
    {
        $this->jFZXRQ = $jFZXRQ;

        return $this;
    }

    /**
     * Get jFZXRQ
     *
     * @return string
     */
    public function getJFZXRQ()
    {
        return $this->jFZXRQ;
    }

    /**
     * Set yJZFJG
     *
     * @param smallint $yJZFJG
     *
     * @return CorrectPersonnel
     */
    public function setYJZFJG($yJZFJG)
    {
        $this->yJZFJG = $yJZFJG;

        return $this;
    }

    /**
     * Get yJZFJG
     *
     * @return bool
     */
    public function getYJZFJG()
    {
        return $this->yJZFJG;
    }

    /**
     * Set yJZFJGMC
     *
     * @param string $yJZFJGMC
     *
     * @return CorrectPersonnel
     */
    public function setYJZFJGMC($yJZFJGMC)
    {
        $this->yJZFJGMC = $yJZFJGMC;

        return $this;
    }

    /**
     * Get yJZFJGMC
     *
     * @return string
     */
    public function getYJZFJGMC()
    {
        return $this->yJZFJGMC;
    }

    /**
     * Set sFYQK
     *
     * @param smallint $sFYQK
     *
     * @return CorrectPersonnel
     */
    public function setSFYQK($sFYQK)
    {
        $this->sFYQK = $sFYQK;

        return $this;
    }

    /**
     * Get sFYQK
     *
     * @return bool
     */
    public function getSFYQK()
    {
        return $this->sFYQK;
    }

    /**
     * Set sFLF
     *
     * @param smallint $sFLF
     *
     * @return CorrectPersonnel
     */
    public function setSFLF($sFLF)
    {
        $this->sFLF = $sFLF;

        return $this;
    }

    /**
     * Get sFLF
     *
     * @return bool
     */
    public function getSFLF()
    {
        return $this->sFLF;
    }

    /**
     * Set qKLX
     *
     * @param string $qKLX
     *
     * @return CorrectPersonnel
     */
    public function setQKLX($qKLX)
    {
        $this->qKLX = $qKLX;

        return $this;
    }

    /**
     * Get qKLX
     *
     * @return string
     */
    public function getQKLX()
    {
        return $this->qKLX;
    }

    /**
     * Set zYFZSS
     *
     * @param string $zYFZSS
     *
     * @return CorrectPersonnel
     */
    public function setZYFZSS($zYFZSS)
    {
        $this->zYFZSS = $zYFZSS;

        return $this;
    }

    /**
     * Get zYFZSS
     *
     * @return string
     */
    public function getZYFZSS()
    {
        return $this->zYFZSS;
    }

    /**
     * Set sQJZQX
     *
     * @param string $sQJZQX
     *
     * @return CorrectPersonnel
     */
    public function setSQJZQX($sQJZQX)
    {
        $this->sQJZQX = $sQJZQX;

        return $this;
    }

    /**
     * Get sQJZQX
     *
     * @return string
     */
    public function getSQJZQX()
    {
        return $this->sQJZQX;
    }

    /**
     * Set sQJZKSRQ
     *
     * @param string $sQJZKSRQ
     *
     * @return CorrectPersonnel
     */
    public function setSQJZKSRQ($sQJZKSRQ)
    {
        $this->sQJZKSRQ = $sQJZKSRQ;

        return $this;
    }

    /**
     * Get sQJZKSRQ
     *
     * @return string
     */
    public function getSQJZKSRQ()
    {
        return $this->sQJZKSRQ;
    }

    /**
     * Set sQJZJSRQ
     *
     * @param string $sQJZJSRQ
     *
     * @return CorrectPersonnel
     */
    public function setSQJZJSRQ($sQJZJSRQ)
    {
        $this->sQJZJSRQ = $sQJZJSRQ;

        return $this;
    }

    /**
     * Get sQJZJSRQ
     *
     * @return string
     */
    public function getSQJZJSRQ()
    {
        return $this->sQJZJSRQ;
    }

    /**
     * Set fZLX
     *
     * @param smallint $fZLX
     *
     * @return CorrectPersonnel
     */
    public function setFZLX($fZLX)
    {
        $this->fZLX = $fZLX;

        return $this;
    }

    /**
     * Get fZLX
     *
     * @return smallint
     */
    public function getFZLX()
    {
        return $this->fZLX;
    }

    /**
     * Set jTZM
     *
     * @param string $jTZM
     *
     * @return CorrectPersonnel
     */
    public function setJTZM($jTZM)
    {
        $this->jTZM = $jTZM;

        return $this;
    }

    /**
     * Get jTZM
     *
     * @return string
     */
    public function getJTZM()
    {
        return $this->jTZM;
    }

    /**
     * Set gZQX
     *
     * @param smallint $gZQX
     *
     * @return CorrectPersonnel
     */
    public function setGZQX($gZQX)
    {
        $this->gZQX = $gZQX;

        return $this;
    }

    /**
     * Get gZQX
     *
     * @return smallint
     */
    public function getGZQX()
    {
        return $this->gZQX;
    }

    /**
     * Set hXKYQX
     *
     * @param smallint $hXKYQX
     *
     * @return CorrectPersonnel
     */
    public function setHXKYQX($hXKYQX)
    {
        $this->hXKYQX = $hXKYQX;

        return $this;
    }

    /**
     * Get hXKYQX
     *
     * @return smallint
     */
    public function getHXKYQX()
    {
        return $this->hXKYQX;
    }

    /**
     * Set sFSZBF
     *
     * @param smallint $sFSZBF
     *
     * @return CorrectPersonnel
     */
    public function setSFSZBF($sFSZBF)
    {
        $this->sFSZBF = $sFSZBF;

        return $this;
    }

    /**
     * Get sFSZBF
     *
     * @return smallint
     */
    public function getSFSZBF()
    {
        return $this->sFSZBF;
    }

    /**
     * Set yPXF
     *
     * @param string $yPXF
     *
     * @return CorrectPersonnel
     */
    public function setYPXF($yPXF)
    {
        $this->yPXF = $yPXF;

        return $this;
    }

    /**
     * Get yPXF
     *
     * @return string
     */
    public function getYPXF()
    {
        return $this->yPXF;
    }

    /**
     * Set yPXQ
     *
     * @param string $yPXQ
     *
     * @return CorrectPersonnel
     */
    public function setYPXQ($yPXQ)
    {
        $this->yPXQ = $yPXQ;

        return $this;
    }

    /**
     * Get yPXQ
     *
     * @return string
     */
    public function getYPXQ()
    {
        return $this->yPXQ;
    }

    /**
     * Set yPXQKSRQ
     *
     * @param string $yPXQKSRQ
     *
     * @return CorrectPersonnel
     */
    public function setYPXQKSRQ($yPXQKSRQ)
    {
        $this->yPXQKSRQ = $yPXQKSRQ;

        return $this;
    }

    /**
     * Get yPXQKSRQ
     *
     * @return string
     */
    public function getYPXQKSRQ()
    {
        return $this->yPXQKSRQ;
    }

    /**
     * Set yPXQJSRQ
     *
     * @param string $yPXQJSRQ
     *
     * @return CorrectPersonnel
     */
    public function setYPXQJSRQ($yPXQJSRQ)
    {
        $this->yPXQJSRQ = $yPXQJSRQ;

        return $this;
    }

    /**
     * Get yPXQJSRQ
     *
     * @return string
     */
    public function getYPXQJSRQ()
    {
        return $this->yPXQJSRQ;
    }

    /**
     * Set yQTXQX
     *
     * @param smallint $yQTXQX
     *
     * @return CorrectPersonnel
     */
    public function setYQTXQX($yQTXQX)
    {
        $this->yQTXQX = $yQTXQX;

        return $this;
    }

    /**
     * Get yQTXQX
     *
     * @return smallint
     */
    public function getYQTXQX()
    {
        return $this->yQTXQX;
    }

    /**
     * Set fJX
     *
     * @param string $fJX
     *
     * @return CorrectPersonnel
     */
    public function setFJX($fJX)
    {
        $this->fJX = $fJX;

        return $this;
    }

    /**
     * Get fJX
     *
     * @return string
     */
    public function getFJX()
    {
        return $this->fJX;
    }

    /**
     * Set sFWD
     *
     * @param string $sFWD
     *
     * @return CorrectPersonnel
     */
    public function setSFWD($sFWD)
    {
        $this->sFWD = $sFWD;

        return $this;
    }

    /**
     * Get sFWD
     *
     * @return string
     */
    public function getSFWD()
    {
        return $this->sFWD;
    }

    /**
     * Set sFWS
     *
     * @param string $sFWS
     *
     * @return CorrectPersonnel
     */
    public function setSFWS($sFWS)
    {
        $this->sFWS = $sFWS;

        return $this;
    }

    /**
     * Get sFWS
     *
     * @return string
     */
    public function getSFWS()
    {
        return $this->sFWS;
    }

    /**
     * Set sFYSS
     *
     * @param string $sFYSS
     *
     * @return CorrectPersonnel
     */
    public function setSFYSS($sFYSS)
    {
        $this->sFYSS = $sFYSS;

        return $this;
    }

    /**
     * Get sFYSS
     *
     * @return string
     */
    public function getSFYSS()
    {
        return $this->sFYSS;
    }

    /**
     * Set sFBXGJZL
     *
     * @param smallint $sFBXGJZL
     *
     * @return CorrectPersonnel
     */
    public function setSFBXGJZL($sFBXGJZL)
    {
        $this->sFBXGJZL = $sFBXGJZL;

        return $this;
    }

    /**
     * Get sFBXGJZL
     *
     * @return smallint
     */
    public function getSFBXGJZL()
    {
        return $this->sFBXGJZL;
    }

    /**
     * Set sQJZRYJSRQ
     *
     * @param string $sQJZRYJSRQ
     *
     * @return CorrectPersonnel
     */
    public function setSQJZRYJSRQ($sQJZRYJSRQ)
    {
        $this->sQJZRYJSRQ = $sQJZRYJSRQ;

        return $this;
    }

    /**
     * Get sQJZRYJSRQ
     *
     * @return string
     */
    public function getSQJZRYJSRQ()
    {
        return $this->sQJZRYJSRQ;
    }

    /**
     * Set bDQK
     *
     * @param string $bDQK
     *
     * @return CorrectPersonnel
     */
    public function setBDQK($bDQK)
    {
        $this->bDQK = $bDQK;

        return $this;
    }

    /**
     * Get bDQK
     *
     * @return string
     */
    public function getBDQK()
    {
        return $this->bDQK;
    }

    /**
     * Set wASBDQKSM
     *
     * @param string $wASBDQKSM
     *
     * @return CorrectPersonnel
     */
    public function setWASBDQKSM($wASBDQKSM)
    {
        $this->wASBDQKSM = $wASBDQKSM;

        return $this;
    }

    /**
     * Get wASBDQKSM
     *
     * @return string
     */
    public function getWASBDQKSM()
    {
        return $this->wASBDQKSM;
    }

    /**
     * Set sFJLJZXZ
     *
     * @param smallint $sFJLJZXZ
     *
     * @return CorrectPersonnel
     */
    public function setSFJLJZXZ($sFJLJZXZ)
    {
        $this->sFJLJZXZ = $sFJLJZXZ;

        return $this;
    }

    /**
     * Get sFJLJZXZ
     *
     * @return smallint
     */
    public function getSFJLJZXZ()
    {
        return $this->sFJLJZXZ;
    }

    /**
     * Set jZXZRYZCQK
     *
     * @param string $jZXZRYZCQK
     *
     * @return CorrectPersonnel
     */
    public function setJZXZRYZCQK($jZXZRYZCQK)
    {
        $this->jZXZRYZCQK = $jZXZRYZCQK;

        return $this;
    }

    /**
     * Get jZXZRYZCQK
     *
     * @return string
     */
    public function getJZXZRYZCQK()
    {
        return $this->jZXZRYZCQK;
    }

    /**
     * Set sFCYDZDWGL
     *
     * @param smallint $sFCYDZDWGL
     *
     * @return CorrectPersonnel
     */
    public function setSFCYDZDWGL($sFCYDZDWGL)
    {
        $this->sFCYDZDWGL = $sFCYDZDWGL;

        return $this;
    }

    /**
     * Get sFCYDZDWGL
     *
     * @return smallint
     */
    public function getSFCYDZDWGL()
    {
        return $this->sFCYDZDWGL;
    }

    /**
     * Set dZDWFS
     *
     * @param smallint $dZDWFS
     *
     * @return CorrectPersonnel
     */
    public function setDZDWFS($dZDWFS)
    {
        $this->dZDWFS = $dZDWFS;

        return $this;
    }

    /**
     * Get dZDWFS
     *
     * @return smallint
     */
    public function getDZDWFS()
    {
        return $this->dZDWFS;
    }

    /**
     * Set dWHM
     *
     * @param string $dWHM
     *
     * @return CorrectPersonnel
     */
    public function setDWHM($dWHM)
    {
        $this->dWHM = $dWHM;

        return $this;
    }

    /**
     * Get dWHM
     *
     * @return string
     */
    public function getDWHM()
    {
        return $this->dWHM;
    }

    /**
     * Set sFTG
     *
     * @param smallint $sFTG
     *
     * @return CorrectPersonnel
     */
    public function setSFTG($sFTG)
    {
        $this->sFTG = $sFTG;

        return $this;
    }

    /**
     * Get sFTG
     *
     * @return smallint
     */
    public function getSFTG()
    {
        return $this->sFTG;
    }

    /**
     * Set jCQK
     *
     * @param smallint $jCQK
     *
     * @return CorrectPersonnel
     */
    public function setJCQK($jCQK)
    {
        $this->jCQK = $jCQK;

        return $this;
    }

    /**
     * Get jCQK
     *
     * @return smallint
     */
    public function getJCQK()
    {
        return $this->jCQK;
    }

    /**
     * Set bZ
     *
     * @param string $bZ
     *
     * @return CorrectPersonnel
     */
    public function setBZ($bZ)
    {
        $this->bZ = $bZ;

        return $this;
    }

    /**
     * Get bZ
     *
     * @return string
     */
    public function getBZ()
    {
        return $this->bZ;
    }

    /**
     * Set cREATEAT
     *
     * @param \DateTime $cREATEAT
     *
     * @return CorrectPersonnel
     */
    public function setCREATEAT($cREATEAT)
    {
        $this->cREATEAT = $cREATEAT;

        return $this;
    }

    /**
     * Get cREATEAT
     *
     * @return \DateTime
     */
    public function getCREATEAT()
    {
        return $this->cREATEAT;
    }

    /**
     * Set uPDATEAT
     *
     * @param \DateTime $uPDATEAT
     *
     * @return CorrectPersonnel
     */
    public function setUPDATEAT($uPDATEAT)
    {
        $this->uPDATEAT = $uPDATEAT;

        return $this;
    }

    /**
     * Get uPDATEAT
     *
     * @return \DateTime
     */
    public function getUPDATEAT()
    {
        return $this->uPDATEAT;
    }

    /**
     * Set iP.
     *
     * @param string|null $iP
     *
     * @return CorrectPersonnel
     */
    public function setIP($iP = null)
    {
        $this->iP = $iP;

        return $this;
    }

    /**
     * Get iP.
     *
     * @return string|null
     */
    public function getIP()
    {
        return $this->iP;
    }

    /**
     * Set sUBMITSTATE.
     *
     * @param int|null $sUBMITSTATE
     *
     * @return CorrectPersonnel
     */
    public function setSUBMITSTATE($sUBMITSTATE = null)
    {
        $this->sUBMITSTATE = $sUBMITSTATE;

        return $this;
    }

    /**
     * Get sUBMITSTATE.
     *
     * @return int|null
     */
    public function getSUBMITSTATE()
    {
        return $this->sUBMITSTATE;
    }

    /**
     * Set sUBMITTIME.
     *
     * @param string|null $sUBMITTIME
     *
     * @return CorrectPersonnel
     */
    public function setSUBMITTIME($sUBMITTIME = null)
    {
        $this->sUBMITTIME = $sUBMITTIME;

        return $this;
    }

    /**
     * Get sUBMITTIME.
     *
     * @return string|null
     */
    public function getSUBMITTIME()
    {
        return $this->sUBMITTIME;
    }

    /**
     * Set sUBMITRESULT.
     *
     * @param string|null $sUBMITRESULT
     *
     * @return CorrectPersonnel
     */
    public function setSUBMITRESULT($sUBMITRESULT = null)
    {
        $this->sUBMITRESULT = $sUBMITRESULT;

        return $this;
    }

    /**
     * Get sUBMITRESULT.
     *
     * @return string|null
     */
    public function getSUBMITRESULT()
    {
        return $this->sUBMITRESULT;
    }

    /**
     * Set sFKSZR.
     *
     * @param int|null $sFKSZR
     *
     * @return CorrectPersonnel
     */
    public function setSFKSZR($sFKSZR = null)
    {
        $this->sFKSZR = $sFKSZR;

        return $this;
    }

    /**
     * Get sFKSZR.
     *
     * @return int|null
     */
    public function getSFKSZR()
    {
        return $this->sFKSZR;
    }

    /**
     * Set sQJZRYJSFS.
     *
     * @param bool $sQJZRYJSFS
     *
     * @return CorrectPersonnel
     */
    public function setSQJZRYJSFS($sQJZRYJSFS)
    {
        $this->sQJZRYJSFS = $sQJZRYJSFS;

        return $this;
    }

    /**
     * Get sQJZRYJSFS.
     *
     * @return bool
     */
    public function getSQJZRYJSFS()
    {
        return $this->sQJZRYJSFS;
    }

    /**
     * Set gUOJIA.
     *
     * @param string $gUOJIA
     *
     * @return CorrectPersonnel
     */
    public function setGUOJIA($gUOJIA)
    {
        $this->gUOJIA = $gUOJIA;

        return $this;
    }

    /**
     * Get gUOJIA.
     *
     * @return string
     */
    public function getGUOJIA()
    {
        return $this->gUOJIA;
    }

    /**
     * Set jZJGSZSFT.
     *
     * @param string|null $jZJGSZSFT
     *
     * @return CorrectPersonnel
     */
    public function setJZJGSZSFT($jZJGSZSFT = null)
    {
        $this->jZJGSZSFT = $jZJGSZSFT;

        return $this;
    }

    /**
     * Get jZJGSZSFT.
     *
     * @return string|null
     */
    public function getJZJGSZSFT()
    {
        return $this->jZJGSZSFT;
    }

    /**
     * Set jZJGSZDSSFJ.
     *
     * @param string|null $jZJGSZDSSFJ
     *
     * @return CorrectPersonnel
     */
    public function setJZJGSZDSSFJ($jZJGSZDSSFJ = null)
    {
        $this->jZJGSZDSSFJ = $jZJGSZDSSFJ;

        return $this;
    }

    /**
     * Get jZJGSZDSSFJ.
     *
     * @return string|null
     */
    public function getJZJGSZDSSFJ()
    {
        return $this->jZJGSZDSSFJ;
    }

    /**
     * Set jZJGSZQXSFJ.
     *
     * @param string|null $jZJGSZQXSFJ
     *
     * @return CorrectPersonnel
     */
    public function setJZJGSZQXSFJ($jZJGSZQXSFJ = null)
    {
        $this->jZJGSZQXSFJ = $jZJGSZQXSFJ;

        return $this;
    }

    /**
     * Get jZJGSZQXSFJ.
     *
     * @return string|null
     */
    public function getJZJGSZQXSFJ()
    {
        return $this->jZJGSZQXSFJ;
    }

    /**
     * Set jZJGSZSFS.
     *
     * @param string|null $jZJGSZSFS
     *
     * @return CorrectPersonnel
     */
    public function setJZJGSZSFS($jZJGSZSFS = null)
    {
        $this->jZJGSZSFS = $jZJGSZSFS;

        return $this;
    }

    /**
     * Get jZJGSZSFS.
     *
     * @return string|null
     */
    public function getJZJGSZSFS()
    {
        return $this->jZJGSZSFS;
    }

    /**
     * Set jZJGSZDSSFJMC.
     *
     * @param string $jZJGSZDSSFJMC
     *
     * @return CorrectPersonnel
     */
    public function setJZJGSZDSSFJMC($jZJGSZDSSFJMC)
    {
        $this->jZJGSZDSSFJMC = $jZJGSZDSSFJMC;

        return $this;
    }

    /**
     * Get jZJGSZDSSFJMC.
     *
     * @return string
     */
    public function getJZJGSZDSSFJMC()
    {
        return $this->jZJGSZDSSFJMC;
    }

    /**
     * Set jZJGSZQXSFJMC.
     *
     * @param string $jZJGSZQXSFJMC
     *
     * @return CorrectPersonnel
     */
    public function setJZJGSZQXSFJMC($jZJGSZQXSFJMC)
    {
        $this->jZJGSZQXSFJMC = $jZJGSZQXSFJMC;

        return $this;
    }

    /**
     * Get jZJGSZQXSFJMC.
     *
     * @return string
     */
    public function getJZJGSZQXSFJMC()
    {
        return $this->jZJGSZQXSFJMC;
    }

    /**
     * Set jZJGSZSFSMC.
     *
     * @param string $jZJGSZSFSMC
     *
     * @return CorrectPersonnel
     */
    public function setJZJGSZSFSMC($jZJGSZSFSMC)
    {
        $this->jZJGSZSFSMC = $jZJGSZSFSMC;

        return $this;
    }

    /**
     * Get jZJGSZSFSMC.
     *
     * @return string
     */
    public function getJZJGSZSFSMC()
    {
        return $this->jZJGSZSFSMC;
    }
    
    /**
     * Set xSTBZT
     *
     * @param integer $xSTBZT
     *
     * @return DevicePersonnel
     */
    public function setXSTBZT($xSTBZT)
    {
        $this->xSTBZT = $xSTBZT;
        
        return $this;
    }
    
    /**
     * Get xSTBZT
     *
     * @return integer
     */
    public function getXSTBZTC()
    {
        return $this->xSTBZT;
    }
    
    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        if ($this->getCREATEAT() == null) {
            $this->setCREATEAT(new \DateTime());
        }
        $this->setUPDATEAT(new \DateTime());
    }
    
    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->setUPDATEAT(new \DateTime());
        $this->setXSTBZT(2);
    }

    /**
     * Get xSTBZT.
     *
     * @return int
     */
    public function getXSTBZT()
    {
        return $this->xSTBZT;
    }

    /**
     * Set jZZT.
     *
     * @param int $jZZT
     *
     * @return CorrectPersonnel
     */
    public function setJZZT($jZZT)
    {
        $this->jZZT = $jZZT;

        return $this;
    }

    /**
     * Get jZZT.
     *
     * @return int
     */
    public function getJZZT()
    {
        return $this->jZZT;
    }

    /**
     * Set gLJB.
     *
     * @param string $gLJB
     *
     * @return CorrectPersonnel
     */
    public function setGLJB($gLJB)
    {
        $this->gLJB = $gLJB;

        return $this;
    }

    /**
     * Get gLJB.
     *
     * @return string
     */
    public function getGLJB()
    {
        return $this->gLJB;
    }
}
