<?php
namespace BlogBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 * User
 */
class User implements UserInterface, \Serializable, AdvancedUserInterface
{
	public function isAccountNonExpired()
	{
		return true;
	}
	
	public function isAccountNonLocked()
	{
		return true;
	}
	
	public function isCredentialsNonExpired()
	{
		return true;
	}
	
	public function isEnabled()
	{
		return $this->isActive;
	}
    /**
     * @var int
     * 
     */
	protected $id;

    /**
     * @var string
	 * @Assert\NotBlank(message="不能为空")
	 * @Assert\Length(min="3",minMessage = "太短了")
     */
	protected $username;

    /**
     * @var string
     */
	protected $password;

	/**
	 * @var int
	 */
	protected $createTime;
	
	/**
	 * @var int
	 */
	protected $isActive;
	
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set createTime
     *
     * @param integer $createTime
     *
     * @return User
     */
    public function setCreateTime($createTime)
    {
        $this->createTime = $createTime;

        return $this;
    }

    /**
     * Get createTime
     *
     * @return int
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }
    
    /**
     *
     * @return isActive
     */
    public function isActive()
    {
    	return $this->isActive;
    }
    
    /**
     * Set username
     *
     * @param string $isActive
     *
     * @return User
     */
    public function setActive($isActive)
    {
    	$this->isActive = $isActive;
    	
    	return $this;
    }
    
    // 将本对象转成数组
    public function toArr(){
    	return get_object_vars($this);
    }
    public function serialize()
    {
    	return serialize(array(
    			$this->id,
    			$this->username,
    			$this->password,
    			$this->createTime,
    			$this->isActive
    	));
    }
    
    public function unserialize($serialized)
    {
    	list (
    		$this->id,
    		$this->username,
    		$this->password,
    		$this->createTime,
    		$this->isActive
   		) = unserialize($serialized);
    }
    
    public function eraseCredentials()
    {}
    
    public function getRoles(){
    	return array('ROLE_USER');
    }
    public function getSalt(){
    	return null;
    }
}

