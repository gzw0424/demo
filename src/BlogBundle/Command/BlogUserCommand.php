<?php

namespace BlogBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class BlogUserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('blog:user')
            ->setDescription('blog id is number (.eq 1,2,3)')
            ->addOption('id', 'i', InputOption::VALUE_OPTIONAL, 'blog id is number (.eq 1,2,3) ')
            ->setHelp('get username of user pass int id argument ');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getOption('id');
        $repo = $this->getContainer()->get('doctrine')->getRepository('BlogBundle:User');
        $user = $repo ->find($id);
        if(empty($user)){
            $output->writeln('没有这个user');
        }else{
            $output->writeln($user->getUsername());
        }
    }
}
