<?php
namespace BlogBundle\Command;

use BlogBundle\Entity\CorrectPersonnel;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ModifyCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('archive:mod:grl') // 命令名称
        ->setDescription('add information.') // 提示
        ->setHelp("This command add information...."); // 帮助
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $arr = [
            '太原' => '0351',
            '朔州' => '0349',
            '忻州' => '0350',
            '大同' => '0352',
            '阳泉' => '0353',
            '晋中' => '0354',
            '长治' => '0355',
            '晋城' => '0356',
            '临汾' => '0357',
            '离石' => '0358',
            '运城' => '0359'
        ];
        // 开始处理
        $start = time();
        $output->writeln([
            '---------------',
            'Start exec'
        ]);

        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine ->getManager();
        $repo = $em->getRepository('BlogBundle:CorrectPersonnel');
        $entity = new CorrectPersonnel();
        $entity = $repo->findAll();
        if(empty($entity)){
            foreach($entity as $value){
                $output->write(count($entity));
            }
        }
        // 处理时间
        $stop = time();
        $mid = $stop-$start;
        $output->writeln([
            '---------------',
            'End flush',
            $mid.'S'
        ]);
    }
}