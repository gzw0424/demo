<?php

namespace BlogBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('user')
            ->setDescription('操作用户的管理器')
            ->addArgument('handle', InputArgument::OPTIONAL, '用户的操作')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
    	$handle = $input->getArgument('handle');
		switch($handle){
			case 'new':
				$this->new($input,$output);
				break;
			case 'del':
				$this->del($input,$output);
				break;
			case 'mod':
				$this->mod($input,$output);
				break;
			case 'find':
				$this->find($input,$output);
				break;
			default:
				$this->notFound($input,$output);
				break;
		}
    }
	
}
