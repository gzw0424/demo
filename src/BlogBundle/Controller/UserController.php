<?php

namespace BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use BlogBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    public function indexAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$users = $em->getRepository('BlogBundle:User')->findAll();
    	return $this->render('BlogBundle:User:index.html.twig', array(
    			'users' => $users,
    	));
    }

    public function addAction(Request $request)
    {
    	$user = new User();
    	$form = $this->createForm('BlogBundle\Form\UserType', $user);
    	$form->handleRequest($request);
    	if ($form->isSubmitted() && $form->isValid()) {
    		$em = $this->getDoctrine()->getManager();
    		$user = $form->getData();
    		$user->setCreateTime(time());
    		$em->persist($user);
    		$em->flush();
    		return $this->redirectToRoute('blog_user_index');
    	}
    	
    	return $this->render('BlogBundle:User:add.html.twig', [
    		'form' => $form->createView(),
    	]);
    }

    public function modifyAction(Request $request)
    {
    	$id = $request->query->get('id');
    	$em = $this->getDoctrine()->getManager();
    	$user = $em->getRepository('BlogBundle:User')->find($id);
    	$form = $this->createForm('BlogBundle\Form\UserType', $user);
    	$form->handleRequest($request);
    	
    	if ($form->isSubmitted() && $form->isValid()) {
    		$em = $this->getDoctrine()->getManager();
    		$user = $form->getData();
    		$user->setCreateTime(time());
    		$em->persist($user);
    		$em->flush();
    		return $this->redirectToRoute('blog_user_index');
    	}
    	
        return $this->render('BlogBundle:User:modify.html.twig', array(
        	'user' => $user,
    		'form' => $form->createView(),
        ));
    }

    public function deleteAction(Request $request)
    {
    	$id = $request->query->get('id');
    	$em = $this->getDoctrine()->getManager();
    	$user = $em->getRepository('BlogBundle:User')->find($id);
    	$em->remove($user);
    	$em->flush();
    	return $this->redirectToRoute('blog_user_index');
		return new Response($this->getEntity($id));
		return new Response($id);
    }
    
}