<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/14
 * Time: 17:57
 */

namespace MessageBundle\Controller;

use Knp\Component\Pager\Event\Subscriber\Paginate\PaginationSubscriber;
use MessageBundle\Service\ControllerCommandService;
use MessageBundle\Service\MessageGenerator;
use Psr\Log\LoggerInterface;
use SensioLabs\AnsiConverter\AnsiToHtmlConverter;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use MessageBundle\Entity\Learn;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Validator\Constraints\DateTime;

class CommandController extends Controller
{
    public function indexAction(Request $request)
    {
        /**
         * @var ControllerCommandService $cc
         */
        $command = $request->request->get('console');
        $conn = null;
//        return new Response($command);
        if(!empty($command)){
            $cc = $this->get('message.controller_command');
            $console = explode(' ',$command);
            $comm = array_shift($console);
            $argu = $this->getArug($console);
//            $cc ->setCommand($command['command']);
//            $cc ->setArguments($command['arguments']);
//            $conn = $cc ->exec();
//            return $this->json($argu);
        }
        return $this->render('MessageBundle:Command:index.html.twig',[
            'conn' => $conn,
        ]);
    }
}