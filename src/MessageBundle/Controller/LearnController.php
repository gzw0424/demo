<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/14
 * Time: 17:57
 */

namespace MessageBundle\Controller;

use Knp\Component\Pager\Event\Subscriber\Paginate\PaginationSubscriber;
use MessageBundle\Service\ControllerCommandService;
use MessageBundle\Service\MessageGenerator;
use Psr\Log\LoggerInterface;
use SensioLabs\AnsiConverter\AnsiToHtmlConverter;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use MessageBundle\Entity\Learn;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Validator\Constraints\DateTime;

class LearnController extends Controller
{
    public function demoAction(Request $request){
        /**
         * @var ControllerCommandService $cc
         */
        $cc = $this->get('message.controller_command');
        $cc ->setCommand('list');
        $cc ->setArguments([]);
        $conn = $cc ->exec();
        return $this->render('MessageBundle:Learn:demo.html.twig',[
            'conn' => $conn,
        ]);
    }
    public function indexAction(Request $request)
    {
        $repo = $this->getDoctrine()->getRepository('MessageBundle:Learn');
        $search = $request->query->get('search');
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $repo->getQuery($search), /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );
        $learns = $pagination->getItems();
        return $this->render('MessageBundle:Learn:index.html.twig',[
            'learns' => $learns,
            'pagination' => $pagination,
        ]);
    }
    public function newAction(Request $request)
    {
        $user = $this->getDoctrine()->getRepository('BlogBundle:User')->findAll();
        if ($request->request->get('submit')) {
            $le = $request->request->get('learn');
            $em = $this->getDoctrine()->getManager();
            $learn = new Learn();
            $learn->setUid($le['uid']);
            $learn->setTitle($le['title']);
            $learn->setContent($le['content']);
            $learn_path = $request->files->get('learn')['path'];
            $learn->setPath($learn_path);
            $mimes = [
                'application/pdf',
            ];
            $validator = $this->get('validator');
            $errors = $validator->validate($learn);
            if(count($errors)){
                return $this->render('MessageBundle:Learn:new.html.twig',[
                    'errors' => $errors
                ]);
            }
            $fileName = $this->get('message.upload_file')->run($learn_path, $mimes, 'image/' . date('Y/m'));
            if($fileName['code'] == 1){
                $learn->setPath($fileName['data']);
                $em->persist($learn);
                $em->flush();
                return $this->redirectToRoute('message_learn_index');
            }else{
                return $this->render('MessageBundle:Learn:new.html.twig',[
                    'errors' => $errors,
                ]);
            }
        }
        return $this->render('MessageBundle:Learn:new.html.twig',[
            'user' => $user
        ]);
    }
    public function showAction(Learn $learn)
    {
        if(empty($learn)){
            return $this->redirectToRoute('message_learn_index');
        }
        return $this->file($learn->getPath(),null,ResponseHeaderBag::DISPOSITION_INLINE);
    }
    public function editAction(Request $request,Learn $learn)
    {
        $user = $this->getDoctrine()->getRepository('BlogBundle:User')->findAll();
        if($request->request->get('submit')){
            $le = $request->request->get('learn');
            $em = $this->getDoctrine()->getManager();
            $learn_path = $request->files->get('learn')['path'];
            $learn->setUid($le['uid']);
            $learn->setTitle($le['title']);
            $learn->setContent($le['content']);
            $mimes = [
                'application/pdf',
            ];
            $path = $learn_path ? $learn_path : $le['path'];
            $learn->setPath($path);
            $validator = $this->get('validator');
            $errors = $validator->validate($learn);
            if(count($errors)){
                return $this->render('MessageBundle:Learn:new.html.twig',[
                    'errors' => $errors,
                    'user' => $user,
                ]);
            }
            if(empty($learn_path) && !empty($le['path'])){
                $em->persist($learn);
                $em->flush();
                return $this->redirectToRoute('message_learn_index');
            }elseif(!empty($learn_path)){
                if($learn_path instanceof UploadedFile){
                    $fileName = $this->get('message.upload_file')->run($learn_path, $mimes, 'image/' . date('Y/m'));
                    if($fileName['code'] == 1){
                        $learn->setPath($fileName['data']);
                        $em->persist($learn);
                        $em->flush();
                        return $this->redirectToRoute('message_learn_index');
                    }
                }
                return $this->render('MessageBundle:Learn:edit.html.twig',[
                    'errors' => $errors,
                    'user' => $user,
                ]);
            } else {
                return $this->render('MessageBundle:Learn:edit.html.twig',[
                    'errors' => $errors,
                    'user' => $user,
                ]);
            }
        }
        return $this->render('MessageBundle:Learn:edit.html.twig',[
            'learn' => $learn,
            'user' => $user,
        ]);
    }
    public function deleteAction(Request $request,Learn $learn)
    {
        $path = $this->getDoctrine()->getRepository('MessageBundle:Learn')->delete($learn)->getPath();
        if(file_exists('../web/'.$path)){
            unlink('../web/'.$path);
        }
        return $this->redirectToRoute('message_learn_index');
    }
    public function detailAction(Request $request)
    {
        $id = $request->query->get('id');
        $learn = $this->getDoctrine()->getRepository('MessageBundle:Learn')->find($id);
        $json = json_encode(['title'=>$learn->getTitle(),'content'=>$learn->getContent()]);
        return new Response($json);
    }
}