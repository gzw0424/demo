<?php

namespace MessageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        $conn = $this->getDoctrine()->getManager('default')->getConnection();
        $learn = $conn ->prepare('select * from information');
        $learn ->execute();
        $learn = $learn->fetchAll();
        dump($learn);
        exit;
    }
    public function batchEdit($entitys)
    {
        $em = $this->getDoctrine()->getManager();
        foreach ($entitys as $item) {
            $item ->setContent('sssssssssssssssssss');
            $em ->persist($item);
        }
        $em->flush();
        return 2;
    }
    public function batchAdd($entity,$length){
        $em = $this->getDoctrine()->getManager();
        for ($i=0; $i<=$length; $i++) {
            $data = new $entity;
            $data ->setUid(3);
            $data ->setTitle('郭子威的测试标题'.$i);
            $data ->setContent('郭子威的测试内容'.$i.'...');
            $data ->setPath('www.sohu.com');
            $em->persist($data);
        }
        $em->flush();
    }
}
