<?php

namespace MessageBundle\Entity;

use BlogBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping\ManyToOne;


/**
 * Learn
 *
 * @ORM\Table(name="learn",options={"commit":"通知公告"})
 * @ORM\Entity(repositoryClass="MessageBundle\Repository\LearnRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Learn
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="uid", type="integer",options={"commit":"用户id"})
     * @Assert\NotBlank(message="用户id不能为空")
     */
    private $uid;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string",length=50,options={"commit":"标题"})
     * @Assert\NotBlank(message="标题不能为空")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text",options={"commit":"文章内容"})
     * @Assert\NotBlank(message="文章内容不能为空")
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255,options={"commit":"附件路径"})
     * @Assert\NotBlank(message="文件不能为空")
     * @Assert\File(
     *              maxSize = "100M",
     *              mimeTypes={ "application/pdf" },
     *              mimeTypesMessage = "PDF格式不支持"
     * )
     */
    private $path;

    /**
     * @var \DateTime
     *
     *
     * @ORM\Column(name="updateat", type="datetimetz")
     */
    private $updateat;

    /**
     * @var \DateTime
     *
     *
     * @ORM\Column(name="createat", type="datetimetz")
     */
    private $createat;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uid
     *
     * @param int $uid
     *
     * @return Learn
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get uid
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Learn
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Learn
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Learn
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set updateat
     *
     * @param \DateTime $updateat
     *
     * @return Learn
     */
    public function setUpdateat($updateat)
    {
        $this->updateat = $updateat;

        return $this;
    }

    /**
     * Get updateat
     *
     * @return \DateTime
     */
    public function getUpdateat()
    {
        return $this->updateat;
    }

    /**
     * Set createat
     *
     * @param \DateTime $createat
     *
     * @return Learn
     */
    public function setCreateat($createat)
    {
        $this->createat = $createat;

        return $this;
    }

    /**
     * Get createat
     *
     * @return \DateTime
     */
    public function getCreateat()
    {
        return $this->createat;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        if($this->getCreateat() == null){
            $this->setCreateat(new \DateTime());
        }
        $this->setUpdateat(new \DateTime());
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->setUpdateat(new \DateTime());
    }
}

