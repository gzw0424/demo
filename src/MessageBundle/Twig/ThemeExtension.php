<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/22
 * Time: 23:53
 */

namespace MessageBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;

class ThemeExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    private $arr = [
        'adsfsaf',
        'bvbdf2a',
        'carw3ga',
    ];
    /**
     * Returns a list of global variables to add to the existing list.
     *
     * @return array An array of global variables
     */
    // 注册一个全局变量
    public function getGlobals()
    {
        return array(
            'theme_name' => $this->getThemeName()
        );
    }

    // 注册一个名为theme的过滤器
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('theme', array($this, 'themeFilter')),
        );
    }

    // 注册一个theme的函数
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('theme', array($this, 'themeFunction')),
        );
    }

    public function themeFilter($index = 5)
    {
        return 'value :'. $this->getThemeName($index);
    }

    public function themeFunction($index = 5)
    {
//        return $this->arr[$index];
        return $this->arr;
    }

    private function getThemeName($index = 2)
    {
        return $this->arr[$index];
    }
}