<?php
namespace MessageBundle\Twig;


class FormErrorsExtension extends \Twig_Extension
{

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('form_errors_tips', array($this,'formErrorsTips')),
        );
    }

    public function formErrorsTips($errors)
    {
        if(!empty($errors)){
            $html = '';
            foreach ($errors as $error) {
                $html .= '<div class="alert alert-yellow">';
                $html .= '<span class="close rotate-hover"></span><strong>提示：' . $error->getMessage();
                $html .= '</div>';
            }
        }else{
            $html = '';
        }
        return $html;
    }
}