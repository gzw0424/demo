<?php
namespace MessageBundle\Twig;


class FormRadiosExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('form_radio', array($this,'formRadio')),
        );
    }

//    raduis实例
//        $radios = [
//            [
//                'attr' => [
//                    'id' => 'cjb1',
//                    'class' => 'BeautifySelect',
//                    'name' => 'correct_personnel[sFDCPG]',
//                    'type' => 'radio',
//                    'value' => '1'
//                ],
//                'text' => '是'
//            ],
//            [
//                'attr' => [
//                    'id' => 'cjb2',
//                    'class' => 'BeautifySelect',
//                    'name' => 'correct_personnel[sFDCPG]',
//                    'value' => '0',
//                    'type' => 'radio',
//                ],
//                'text' => '是'
//            ]
//        ];
    public function formRadio($radios,$infos)
    {
        $html = '';
        if(!empty($radios)){
            foreach ($radios as $radio) {
                $attr = '';
                $eqname = explode(']',explode('[',$radio['attr']['name'])[1])[0];
                if(!empty($infos) && ($infos[$eqname]==$radio['attr']['value'])){
                    $check = 'checked';
                }
                foreach ($radio['attr'] as $key=>$value) {
                    $attr .= $key.'="'.$value.'" ';
                }
                $text = $radio['text'];
                $html .= '<input '.$attr.' '.$check.'/>';
                $html .= '<label for="'.$radio['attr']['id'].'">'.$text.'</label>';
            }
        }
        return $html;
    }
}