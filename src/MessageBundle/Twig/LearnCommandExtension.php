<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/29
 * Time: 19:07
 */

namespace MessageBundle\Twig;

use SensioLabs\AnsiConverter\AnsiToHtmlConverter;

class LearnCommandExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('learn_command', array($this,'learnCommand')),
        );
    }

    public function learnCommand($content)
    {
        $converter = new AnsiToHtmlConverter();
        return $converter->convert($content);
    }
}