<?php

namespace MessageBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;

class MessageLearnCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('message:learn')
//            ->setHidden(true)
            ->setDescription('learn id is number (.eq 1,2,3)')
            ->addOption('id', 'i', InputOption::VALUE_OPTIONAL, 'learn id is number (.eq 1,2,3) ')
            ->setHelp('get title of learn pass int id argument ');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getOption('id');
        $repo = $this->getContainer()->get('doctrine')->getRepository('MessageBundle:Learn');
        $learn = $repo ->find($id);
        $output->writeln('<error>'.$learn->getTitle().'</error>');
        $command = $this->getApplication()->find('blog:user');
        $arguments = array(
            '--id' => $id,
        );
        $greetInput = new ArrayInput($arguments);
        $returnCode = $command->run($greetInput, $output);
    }

}