<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/27
 * Time: 11:27
 */

namespace MessageBundle\Service;


class FormRequest
{
    protected function handleRequest(Request $request, $entity, $tableName, $entityObject = null)
    {
        if(is_null($entityObject)){
            $entityObject = new $entity();
        }
        $post = $request->request->get($tableName);
        foreach ($post as $key => $val) {
            $method = 'set' . ucwords($key, '_');
            if(method_exists($entity, $method)){
                $entityObject->$method($val);
            }
        }

        return $entityObject;
    }
}