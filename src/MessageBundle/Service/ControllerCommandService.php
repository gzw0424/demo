<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/27
 * Time: 11:27
 */

namespace MessageBundle\Service;


use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpKernel\KernelInterface;

class ControllerCommandService
{
    private $kernel = null;
    private $command = null;
    private $arguments = null;
    public function __construct(KernelInterface $kernel,$command,$arguments)
    {
        $this->kernel = $kernel;
        $this->command = $command;
        $this->arguments = $arguments;
    }

    /**
     * @return null
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * @param null $command
     */
    public function setCommand($command)
    {
        $this->command = $command;
    }

    /**
     * @return array|null
     */
    public function getArguments()
    {
        return $this->arguments;
    }

    /**
     * @param array|null $arguments
     */
    public function setArguments($arguments)
    {
        $this->arguments = $arguments;
    }

    public function exec()
    {
        $application = new Application($this->kernel);
        $application->setAutoExit(false);
        $input = new ArrayInput(array_merge(array(
            'command' => $this->command,
        ),$this->arguments));
        $output = new BufferedOutput();
        $application->run($input,$output);
        $conn = $output->fetch();
        return $conn;
    }
}