<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/20
 * Time: 10:05
 */

namespace MessageBundle\Service;
use Psr\Log\LoggerInterface;


class MessageGenerator
{
    private $logger;
    private $index;
    private $start;
    private $end;

    public function __construct(LoggerInterface $logger,$index,$start,$end)
    {
        $this->logger = $logger;
        $this->index = $index;
        $this->start = $start;
        $this->end = $end;
    }

    public function getHappyMessage()
    {
        $index = $this->index;
        if(is_int($index) && $index<0){
//            return false;
            return json_encode([
                'code' => 101,
                'msg' => '这个index值必须是一个大于0的整数'
            ]);
        }
        $arr = range($this->start,$this->end);
        if(empty($arr)){
//            return false;
            return json_encode([
                'code' => 102,
                'msg' => '创建的数组为空！请检查你给的start和end'
            ]);
        }
        $arr = array_filter($arr,function($value) use ($index){
            if(0 == ($value%$index)){
                return true;
            }
        });
        return json_encode([
            'code' => 1,
            'msg' => '创建数组成功！',
            'arr' => $arr
        ]);
//        return $arr;
    }
}