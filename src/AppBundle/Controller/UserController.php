<?php
namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller{
	public function indexAction(Request $request){
		$user = new User();
		$form = $this->createForm('AppBundle\Form\UserType', $user);
		$form->handleRequest($request);
		if($form->isSubmitted()){
			if($form->isValid()){
				$encoder = $this->get('security.password_encoder');
				$user = $form->getData();
				$em = $this->getDoctrine()->getManager();
				if($em->getRepository('AppBundle:User')->validRepetition($user->getUsername())){
					return new Response('重复用户名:'.$user->getUsername());
				}else{
					$user->setUsername('ss');
					$em->persist($user);
					$em->flush();
					return $this->redirectToRoute('admin_login_index');
				}
			}
		}
		return $this->render('User/index.html.twig', array(
				'form' => $form->createView(),
				'users'=> $user
		));
	}
	
	public function createAction(){
		$em = $this->getDoctrine()->getManager();
		$user = new User();
		$user->setUsername('gzw0424'.mt_rand(0,100));
		$user->setPassword('9090681');
		$user->setCreateTime(time());
		$em->persist($user);
		$em->flush();
		return new Response($user->getId());
	}
	public function showAction($id)
	{
		$user = $this->getDoctrine()
			->getRepository(User::class)
			->findBy([
				'password'=>'9090681'
			]);
		if (!$user) {
			throw $this->createNotFoundException(
				'id是'.$id
			);
		}else{
// 			return new Response($user->getUsername());
			$str = '';
			foreach ($user as $u){
				$str .= $u->getUsername();
			}
			return new Response($str);
		}		
	}
	public function updateAction($id)
	{
		$em = $this->getDoctrine()->getManager();
		$user = $em->getRepository(User::class)->find($id);
		
		if (!$user) {
			throw $this->createNotFoundException(
				'No product found for id '.$id
			);
		}
		
		$user->setUsername('New product name!');
		$em->flush();
		
		return new Response('ok');
	}
	public function deleteAction($id){
		$em = $this->getDoctrine()->getManager();
		$user = $em->getRepository(User::class)->find($id);
		if (!$user) {
			throw $this->createNotFoundException(
					'No product found for id '.$id
					);
		}
		$em->remove($user);
		$em->flush();
		return new Response('delete name :'.$user->getUsername());
	}
	//dql查询
	public function dqlAction($id){
		$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery(
			'SELECT u.username FROM AppBundle:User u WHERE u.createTime < :ct ORDER BY u.createTime'
		)->setParameter('ct',time());
		$user = $query->getResult();
		$str = '';
		foreach ($user as $u){
			$str .= $u['username'];
		}
		return new Response($str);
	}
	//用qb进行dql查询
	public function qbdqlAction(){
		$repository = $this->getDoctrine()->getRepository(User::class);
		$query = $repository->createQueryBuilder('u')
			->where('u.createTime > :ct')
			->setParameter('ct',1000000)
			->getQuery();
		$user = $query->getResult();
		$str = '';
		foreach ($user as $u){
			$str .= $u->getUsername();
		}
		return new Response($str);
	}
}