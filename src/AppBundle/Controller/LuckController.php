<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Psr\Log\LoggerInterface;

class LuckController extends Controller
{
	public function numberAction(LoggerInterface $logger)
	{
		$number = mt_rand(0,100);
		$logger->info('luck number is '.$number);
		return $this->render('luck/number.html.twig',[
			'number'=>$number,
		]);
	}
}
