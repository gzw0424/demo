<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Task;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class TaskController extends Controller
{
	public function newAction(Request $request)
	{
		// create a task and give it some dummy data for this example
		
		$form = $this->createFormBuilder($task)
		->add('task', TextType::class)
		->add('dueDate', DateType::class)
		->add('save', SubmitType::class, array('label' => '提交'))
		->getForm();
		$form->handleRequest($request);
		if($form->isSubmitted() && $form->isValid()){
			$task = new Task();
			$task->setTask('hhhhhhhhh');
			$task->setDueDate(new \DateTime('2018-4-5'));
			
			$task = $form->getData();
			$em = $this->getDoctrine()->getManager();
			$em->persist($task);
			$em->flush();
			$this->redirect('task_new');
		}
		$tasks = $this->getDoctrine()
			->getRepository(Task::class)
			->findAll();
		
		return $this->render('Task/index.html.twig', [
			'form' => $form->createView(),
			'tasks' => $tasks
		]);
	}
}