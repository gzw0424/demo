<?php
namespace AppBundle\Entity;

class Task
{
	private $id;
	private $task;
	private $dueDate;
	
	public function getId()
	{
		return $this->id;
	}
	
	public function getTask()
	{
		return $this->task;
	}
	
	public function setTask($task)
	{
		$this->task = $task;
	}
	
	public function getDueDate()
	{
		return $this->dueDate;
	}
	
	public function setDueDate(\DateTime $dueDate = null)
	{
		$this->dueDate = $dueDate;
	}
	
	public function toArr(){
		return get_object_vars($this);
	}
}
